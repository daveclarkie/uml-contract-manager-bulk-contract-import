Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Configuration.ConfigurationManager

Partial Class helpdesk_budgetupload
    Inherits System.Web.UI.Page

    Dim strFile As String = "data\EMPS Budget Bulk Upload 2.csv"

    Dim ImportDescription As String = "Bulk Import - EMPS Budget Bulk Upload"

    Dim strnow As String = Now.Date.ToString("dd MMM yyyy")

    Dim Subject As String = ""
    Dim Customer As String = ""
    Dim Country As String = ""
    Dim Commodity As String = ""
    Dim BudgetFrom As Date = Nothing
    Dim BudgetTo As Date = Nothing
    Dim Requestor As String = ""
    Dim Technician As String = ""
    Dim CBMSBudgetID As Integer = -1
    Dim Reforecast As String = ""
    Dim ScheduledDate As Date = Nothing
    Dim InputsFinalisedDate As Date = Nothing
    Dim PriceDate As Date = Nothing
    Dim DueDate As Date = Nothing
    Dim ConsumptionMeth As String = ""
    Dim CommodityMeth As String = ""
    Dim OtherComments As String = ""

    Dim helpdesk_id As Integer = -1
    Dim strXML As String = ""

    Dim mCSV As New CSVData

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.lbloutput.Text = ImportDescription
        Try
            ResetVariables()

        Catch aa As Exception
            System.Diagnostics.Debug.WriteLine(aa.Message.ToString)
        End Try


    End Sub

    Public Sub LoadX()
        '

        ' Set the separator and text qualifier

        mCSV.Separator = ","
        mCSV.TextQualifier = """"
        '
        ' Load in the CSV
        '
        mCSV.LoadCSV(Server.MapPath(strFile), True)
        '
        ' Clear the listview of any data and columns
        '
        Dim maxcol As Integer = mCSV.CSVDataSet.Tables(0).Columns.Count - 1
        Dim dt As DataTable = mCSV.CSVDataSet.Tables(0)
        ProcessBulkDataTable(dt)

    End Sub

    Private Sub ResetVariables()

        Subject = ""
        Customer = ""
        Country = ""
        Commodity = ""
        BudgetFrom = Nothing
        BudgetTo = Nothing
        Requestor = ""
        Technician = ""
        CBMSBudgetID = -1
        Reforecast = ""
        ScheduledDate = Nothing
        InputsFinalisedDate = Nothing
        PriceDate = Nothing
        DueDate = Nothing
        ConsumptionMeth = ""
        CommodityMeth = ""
        OtherComments = ""

        helpdesk_id = -1
        strXML = ""


    End Sub

    Private Function getfirst(ByVal str As String) As String
        Dim out As String = ""

        Dim chrPos As Integer = 0
        Dim chrMax As Integer = str.Length

        Do While chrPos < chrMax
            If chrPos > 0 Then
                If Char.IsUpper(str, chrPos) Then
                    If Char.IsLower(str, chrPos - 1) Then
                        Exit Do
                    End If
                    If str(chrPos - 1).ToString = ")" Then
                        Exit Do
                    End If
                    If Char.IsDigit(str, chrPos - 1) Then
                        If Not Char.IsWhiteSpace(str, chrPos + 1) Then
                            Exit Do
                        End If
                    End If
                End If
                If Char.IsDigit(str, chrPos) Then
                    If Char.IsLower(str, chrPos - 1) Then
                        Exit Do
                    End If
                End If
                If str(chrPos).ToString = "," Then
                    Exit Do
                End If
            End If
            out += str(chrPos).ToString
            chrPos += 1
        Loop
        Return out
    End Function

    Private Sub ProcessBulkDataTable(ByVal dt As DataTable)

        System.Diagnostics.Debug.WriteLine(ImportDescription)

        Dim dr As DataRow
        Dim iRowIndex As Integer = 0
        For Each dr In dt.Rows
            iRowIndex += 1
            ResetVariables()

            If Not IsDBNull(dr(1)) Then

                Subject = dr("Subject").ToString.Replace("'", "")
                Customer = dr("Customer").ToString.Replace("'", "")
                Country = dr("Country").ToString.Replace("'", "")
                Commodity = dr("Commodity").ToString.Replace("'", "")
                BudgetFrom = CDate(dr("Budget From"))
                BudgetTo = CDate(dr("Budget To"))
                Requestor = dr("Requestor").ToString.Replace("'", "")
                Technician = dr("Technician").ToString.Replace("'", "")
                CBMSBudgetID = dr("CBMS Budget ID").ToString.Replace("'", "")
                Reforecast = dr("Reforecast?").ToString.Replace("'", "")
                ScheduledDate = CDate(dr("Scheduled Date"))
                InputsFinalisedDate = CDate(dr("Inputs Finalised Date"))

                PriceDate = CDate(dr("Price Date"))
                DueDate = CDate(dr("Due Date"))
                ConsumptionMeth = dr("Consumption Meth").ToString.Replace("'", "")
                CommodityMeth = dr("Commodity Meth").ToString.Replace("'", "")
                OtherComments = dr("Other Comments").ToString.Replace("'", "")


                Requestor = "Dave Clarke"
                Technician = "Dave Clarke"

                SetVariables()

                System.Diagnostics.Debug.WriteLine(iRowIndex & " / " & dt.Rows.Count & " (" & (iRowIndex / dt.Rows.Count) * 100 & " %)")
                Me.lbloutput.Text &= "<br />" & iRowIndex & " | " & " | helpdesk_id: " & helpdesk_id & " | Subject: " & Subject & " | XML: " & strXML & ")"

            End If
        Next

    End Sub

    Private Sub SetVariables()

        strXML &= AppSettings("HelpdeskURL") & AppSettings("APIURL")
        strXML &= "?OPERATION_NAME=ADD_REQUEST&TECHNICIAN_KEY=" & AppSettings("RestAPIKey") & "&INPUT_DATA="
        strXML &= "<Operation>"
        strXML &= "<Details>"



        strXML &= "<parameter>"
        strXML &= "<name>requesttemplate</name>"
        strXML &= "<value>Budget Requests</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Subject</name>"
        strXML &= "<value>" & Subject & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Customer</name>"
        strXML &= "<value>" & Customer & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Country Code</name>"
        strXML &= "<value>" & Country & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Commodity</name>"
        strXML &= "<value>" & Commodity & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Budget From</name>"
        strXML &= "<value>" & BudgetFrom & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Budget To</name>"
        strXML &= "<value>" & BudgetTo & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Requestor</name>"
        strXML &= "<value>" & Requestor & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Technician</name>"
        strXML &= "<value>" & Technician & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>CBMS Budget ID</name>"
        strXML &= "<value>" & CBMSBudgetID & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Reforecast?</name>"
        strXML &= "<value>" & Reforecast & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>CBMS Volumes Finalised Date</name>"
        strXML &= "<value>" & InputsFinalisedDate & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Desired Price Date</name>"
        strXML &= "<value>" & PriceDate & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Desired Due Date</name>"
        strXML &= "<value>" & DueDate & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Consumption Methodology</name>"
        strXML &= "<value>" & ConsumptionMeth & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Commodity Methodology</name>"
        strXML &= "<value>" & CommodityMeth & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Description</name>"
        strXML &= "<value>" & OtherComments & "</value>"
        strXML &= "</parameter>"

        strXML &= "</Details>"
        strXML &= "</Operation>"


        'strXML &= "<parameter>"
        'strXML &= "<name></name>"
        'strXML &= "<value></value>"
        'strXML &= "</parameter>"

        System.Diagnostics.Debug.WriteLine(strXML)
        
    End Sub

    Protected Sub btnRunImport_Click(sender As Object, e As EventArgs) Handles btnRunImport.Click
        LoadX()
    End Sub
End Class
