Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class _gas_contract
    Inherits System.Web.UI.Page

    Dim strFile As String = "data\EAS-4628.csv"
    Dim strConnection As String = "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=UML_CMS;Trusted_Connection=True;"
    Dim CommandText As String = ""
    Dim myConnection As SqlConnection
    Dim CO As SqlCommand

    Dim ImportDescription As String = Left("Bulk Import - " & strFile.ToString.Replace("data\", ""), 50)

    Dim strnow As String = Now.Date.ToString("dd MMM yyyy")

    Dim site_id As String = ""

    Dim fuel As String = ""
    Dim read_frequency As String = ""
    Dim start_date As String = ""
    Dim end_date As String = ""

    Dim supplier_name As String = ""

    Dim rate_allunit As String = ""
    Dim rate_day As String = ""
    Dim rate_night As String = ""

    Dim usage_allunit As String = ""
    Dim usage_day As String = ""
    Dim usage_night As String = ""

    Dim standingcharge_month As String = ""
    Dim commission_kwh As String = ""
    Dim commission_year As String = ""

    Dim contract_type As String = ""
    Dim tender_status As String = ""
    Dim termination_optout As String = ""
    Dim registration_confirmed As String = ""

    Dim contract_id As String = ""


    Dim tenderid As Integer = -1
    Dim rateid As Integer = -1
    Dim contractid As Integer = -1
    Dim supplydetailid As Integer = -1



    Dim mCSV As New CSVData

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.lbloutput.Text = ImportDescription
        Try
            ResetVariables()

            If lblBeforeCustomers.Text = 0 Then
                CommandText = "SELECT count(*) FROM tblCustomer"
                myConnection = New SqlConnection(strConnection)
                CO = New SqlCommand(CommandText, myConnection)
                myConnection.Open()
                lblBeforeCustomers.Text = Convert.ToString(CO.ExecuteScalar())
            End If

        Catch aa As Exception
            System.Diagnostics.Debug.WriteLine(aa.Message.ToString)
        End Try


    End Sub

    Public Sub LoadX()
        '

        ' Set the separator and text qualifier

        mCSV.Separator = ","
        mCSV.TextQualifier = """"
        '
        ' Load in the CSV
        '
        mCSV.LoadCSV(Server.MapPath(strFile), True)
        '
        ' Clear the listview of any data and columns
        '
        Dim maxcol As Integer = mCSV.CSVDataSet.Tables(0).Columns.Count - 1
        Dim dt As DataTable = mCSV.CSVDataSet.Tables(0)
        ProcessBulkDataTable(dt)

    End Sub

    Private Sub ResetVariables()

        site_id = ""
        
        fuel = ""
        start_date = ""
        end_date = ""
        read_frequency = ""

        supplier_name = ""
        
        rate_allunit = ""
        rate_day = ""
        rate_night = ""

        usage_allunit = ""
        usage_day = ""
        usage_night = ""

        standingcharge_month = ""
        commission_kwh = ""
        commission_year = ""

        contract_type = ""
        tender_status = ""
        termination_optout = ""
        registration_confirmed = ""


        contract_id = ""


        tenderid = -1
        rateid = -1
        contractid = -1
        supplydetailid = -1
    End Sub

    Private Function getfirst(ByVal str As String) As String
        Dim out As String = ""

        Dim chrPos As Integer = 0
        Dim chrMax As Integer = str.Length

        Do While chrPos < chrMax
            If chrPos > 0 Then
                If Char.IsUpper(str, chrPos) Then
                    If Char.IsLower(str, chrPos - 1) Then
                        Exit Do
                    End If
                    If str(chrPos - 1).ToString = ")" Then
                        Exit Do
                    End If
                    If Char.IsDigit(str, chrPos - 1) Then
                        If Not Char.IsWhiteSpace(str, chrPos + 1) Then
                            Exit Do
                        End If
                    End If
                End If
                If Char.IsDigit(str, chrPos) Then
                    If Char.IsLower(str, chrPos - 1) Then
                        Exit Do
                    End If
                End If
                If str(chrPos).ToString = "," Then
                    Exit Do
                End If
            End If
            out += str(chrPos).ToString
            chrPos += 1
        Loop
        Return out
    End Function

    Private Sub ProcessBulkDataTable(ByVal dt As DataTable)

        Dim dr As DataRow
        Dim iRowIndex As Integer = 0
        For Each dr In dt.Rows
            iRowIndex += 1
            ResetVariables()

            site_id = dr("site_id").ToString.Replace("'", "")
            
            fuel = dr("fuel").ToString.Replace("'", "")
            start_date = dr("start_date").ToString.Replace("'", "")
            end_date = dr("end_date").ToString.Replace("'", "")
            
            supplier_name = dr("supplier_name").ToString.Replace("'", "")
            
            rate_allunit = dr("rate_allunit").ToString.Replace("'", "")
            rate_day = dr("rate_day").ToString.Replace("'", "")
            rate_night = dr("rate_night").ToString.Replace("'", "")

            usage_allunit = dr("usage_allunit").ToString.Replace("'", "")
            usage_day = dr("usage_day").ToString.Replace("'", "")
            usage_night = dr("usage_night").ToString.Replace("'", "")

            standingcharge_month = dr("standingcharge_month").ToString.Replace("'", "")
            commission_kwh = dr("commission_kwh").ToString.Replace("'", "")
            commission_year = dr("commission_year").ToString.Replace("'", "")

            contract_type = dr("contract_type").ToString.Replace("'", "")
            tender_status = dr("tender_status").ToString.Replace("'", "")
            termination_optout = dr("termination_optout").ToString.Replace("'", "")
            registration_confirmed = dr("registration_confirmed").ToString.Replace("'", "")


            contract_id = dr("contract_id").ToString.Replace("'", "")


            If Len(contract_id) = 0 Then

                Dim part_contracttype As String() = contract_type.Split(" ")
                Dim part_supplier As String() = supplier_name.Split(" ")
                Dim part_month As String = CStr(MonthName(DatePart(DateInterval.Month, CDate(end_date)), True))
                Dim part_year As String = CStr(DatePart(DateInterval.Year, CDate(end_date)))
                Dim part_contracttypeoutput As String = ""
                If part_contracttype(0) = "Fixed" Then
                    part_contracttypeoutput = "Fixed"
                ElseIf part_contracttype(0) = "Flexible" Then
                    part_contracttypeoutput = "Flex"
                End If

                contract_id = CStr(part_contracttypeoutput & "_" & part_supplier(0) & "_" & part_month & part_year)
                contract_id = contract_id.ToLower

            End If

            SetVariables()
            If tenderid = -1 Then
                'System.Diagnostics.Debug.WriteLine(iRowIndex & " | tender: " & tenderid & " | history: " & contractid & " (" & iRowIndex & "/" & dt.Rows.Count & ")")
                SetVariables()
                'System.Diagnostics.Debug.WriteLine(iRowIndex & " | tender: " & tenderid & " | history: " & contractid & " (" & iRowIndex & "/" & dt.Rows.Count & ")")
            End If

            System.Diagnostics.Debug.WriteLine(iRowIndex & " / " & dt.Rows.Count & " (" & (iRowIndex / dt.Rows.Count) * 100 & " %)")
            Me.lbloutput.Text &= "<br />" & iRowIndex & " | " & site_id & " | " & supplier_name & " | " & tenderid & " (" & iRowIndex & "/" & dt.Rows.Count & ")"

          
        Next

        CommandText = "SELECT count(*) FROM tblCustomer"
        myConnection = New SqlConnection(strConnection)
        CO = New SqlCommand(CommandText, myConnection)
        myConnection.Open()
        lblAfterCustomers.Text = Convert.ToString(CO.ExecuteScalar())

    End Sub

    Private Sub SetVariables()


        myConnection = New SqlConnection(strConnection)
        myConnection.Open()
        CO = New SqlCommand

        Dim siteid As Integer = -1
        Dim supplierid As Integer = -1
        Dim contractypeid As Integer = -1

        'supplier name
        CO.CommandType = CommandType.Text
        CommandText = "SELECT SupplierId FROM [UML_CMS].[dbo].[tblsuppliers] WHERE Supplier_name = '" & supplier_name & "'"
        CO = New SqlCommand(CommandText, myConnection)
        supplierid = CInt(CO.ExecuteScalar)
        If Not supplierid > 0 Then
            supplierid = -1
        End If

        'contract type
        CO.CommandType = CommandType.Text
        CommandText = "SELECT ID FROM dbo.tbl_Purchasing_BasketAdministration WHERE iType = 'G' AND BasketDescription = '" & contract_type & "'"
        CO = New SqlCommand(CommandText, myConnection)
        contractypeid = CInt(CO.ExecuteScalar)
        If Not contractypeid > 0 Then
            contractypeid = -1
        End If


        Dim tenderproductid As Integer = contractypeid

        ' try and match sites
        siteid = site_id


        If siteid > 0 Then ' if there is a site

            ' sets tender complete
            CO = New SqlCommand
            CO.CommandType = CommandType.Text
            CommandText = "UPDATE tblsites SET statusid = 4 WHERE siteid = " & siteid
            CO = New SqlCommand(CommandText, myConnection)
            CO.ExecuteNonQuery()

            ' contract stuff

            Select Case supplierid
                Case Nothing
                    supplierid = 42
            End Select

            CommandText = "SELECT PKgasratehistid FROM [UML_CMS].[dbo].[tblgasratehistory]"
            CommandText &= " WHERE fksiteid = " & siteid
            CommandText &= " and ratestdate = '" & CDate(start_date).ToString("dd MMM yyyy") & "'"
            CommandText &= " and ctrctacnum = '" & contract_id & "'"
            CO = New SqlCommand(CommandText, myConnection)
            contractid = CInt(CO.ExecuteScalar)

            ' tidy up the Usage's
            usage_day = usage_day.Replace(",", "")
            usage_night = usage_night.Replace(",", "")
            usage_allunit = usage_allunit.Replace(",", "")

            If usage_day = "N/A" Or usage_day = "" Then : usage_day = 0 : End If
            If usage_night = "N/A" Or usage_night = "" Then : usage_night = 0 : End If
            If usage_allunit = "N/A" Or usage_allunit = "" Then : usage_allunit = 0 : End If

            If usage_day.Length > 0 And Not IsNumeric(Left(usage_day, 1)) Then : usage_day = usage_day.Replace(Left(usage_day, 1), "") : End If
            If usage_night.Length > 0 And Not IsNumeric(Left(usage_night, 1)) Then : usage_night = usage_night.Replace(Left(usage_night, 1), "") : End If
            If usage_allunit.Length > 0 And Not IsNumeric(Left(usage_allunit, 1)) Then : usage_allunit = usage_allunit.Replace(Left(usage_allunit, 1), "") : End If

            usage_day = CInt(usage_day)
            usage_night = CInt(usage_night)
            usage_allunit = CInt(usage_allunit)


            If rate_allunit.Length = 0 Then rate_allunit = "0"
            If rate_day.Length = 0 Then rate_day = "0"
            If rate_night.Length = 0 Then rate_night = "0"

            If standingcharge_month.Length = 0 Then standingcharge_month = "0"
            If commission_kwh.Length = 0 Then commission_kwh = "0"
            If commission_year.Length = 0 Then commission_year = "0"

            'contract doesnt already exist
            If contractid < 1 Then
                CO = New SqlCommand
                CommandText = "INSERT INTO [UML_CMS].[dbo].[tblgastenders]"
                CommandText &= "([siteid],[tenderdesc], [tenderstatus], [wonlost], [tenderclsddate], [FKtenderproduct])"

                CommandText &= " VALUES "
                CommandText &= " (" & siteid & ", '" & ImportDescription & "', 1, 1, '" & CDate(strnow).ToString("dd MMM yyyy") & "', " & tenderproductid & ");"
                CommandText &= "SELECT @@IDENTITY;"
                CO = New SqlCommand(CommandText, myConnection)
                tenderid = CInt(CO.ExecuteScalar)

                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "INSERT INTO [UML_CMS].[dbo].[tblgasrates]"
                CommandText &= "([gastenderid],[supplierid],[gastendrate],[ctrctlength],[gasmeterchgpa],[suppliercommision],[supplierfixedcomm]) "
                CommandText &= " VALUES "
                CommandText &= " (" & tenderid & ", " & supplierid & ", '" & rate_allunit & "', DATEDIFF(MONTH, '" & CDate(start_date).ToString("dd MMM yyyy") & "','" & CDate(end_date).ToString("dd MMM yyyy") & "')+1, '" & standingcharge_month & "', '" & commission_kwh & "','" & CInt(commission_year) & "');"
                CommandText &= "SELECT @@IDENTITY;"
                CO = New SqlCommand(CommandText, myConnection)
                rateid = CInt(CO.ExecuteScalar)

                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "INSERT INTO [UML_CMS].[dbo].[tblgasratehistory]"
                CommandText &= "([fksiteid],[rateid],[tenderid],[ratestdate],[rateenddate],[ctrctacnum],[regconfirmed]) "
                CommandText &= "VALUES("
                CommandText &= siteid & ", " & rateid & ", " & tenderid & ", '" & CDate(start_date).ToString("dd MMM yyyy") & "', '" & CDate(end_date).ToString("dd MMM yyyy") & "','" & contract_id & "'"
                If registration_confirmed = "Yes" Then
                    CommandText &= ",1);"
                Else
                    CommandText &= ",0);"
                End If
                CommandText &= "SELECT @@IDENTITY;"
                CO = New SqlCommand(CommandText, myConnection)
                contractid = CInt(CO.ExecuteScalar)


                'supply detail already exists for site
                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "SELECT gassupplyid FROM [UML_CMS].[dbo].[tblgassupplydetails] WHERE [siteid] = " & siteid
                CO = New SqlCommand(CommandText, myConnection)
                supplydetailid = CInt(CO.ExecuteScalar)

                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "SELECT StatusID FROM [UML_CMS].[dbo].[tbltenderstatus] WHERE [StatusMain] = '" & tender_status & "'"
                CO = New SqlCommand(CommandText, myConnection)
                Dim tenderstatusid As Integer = -1
                tenderstatusid = CInt(CO.ExecuteScalar)


                If supplydetailid = 0 Then
                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "INSERT INTO [UML_CMS].[dbo].[tblgassupplydetails]"
                    CommandText &= "([siteid],[nominatedkwh],[ctrctstatus], [DMorNDM]) "
                    CommandText &= "VALUES("
                    CommandText &= siteid & ", '" & usage_allunit & "', " & tenderstatusid & ", '" & read_frequency & "');"
                    CommandText &= "SELECT @@IDENTITY;"
                    CO = New SqlCommand(CommandText, myConnection)
                    supplydetailid = CInt(CO.ExecuteScalar)

                    Dim meterid As Integer = -1
                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "SELECT mnumid FROM tblmnumbers where siteid = " & siteid & " and valid_from <= getdate() and (valid_to >= getdate() OR valid_to IS NULL)"
                    CO = New SqlCommand(CommandText, myConnection)
                    meterid = CInt(CO.ExecuteScalar)

                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "INSERT INTO [UML_CMS].[dbo].[tblgassupplyaqs]"
                    CommandText &= "([creator_fk], [editor_fk], [meter_fk], [annualquantity]) "
                    CommandText &= "VALUES(349,349, " & meterid & ", '" & usage_allunit & "');"
                    CommandText &= "SELECT @@IDENTITY;"
                    CO = New SqlCommand(CommandText, myConnection)
                    CO.ExecuteNonQuery()
                Else
                    Dim kwh As Double = -1
                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "SELECT isnull(nominatedkwh,0) FROM [UML_CMS].[dbo].[tblgassupplydetails]"
                    CommandText &= " WHERE [gassupplyid] = " & supplydetailid
                    CO = New SqlCommand(CommandText, myConnection)
                    kwh = CDbl(CO.ExecuteScalar)
                    If usage_allunit <> kwh And usage_allunit > 0 Then
                        CO = New SqlCommand
                        CO.CommandType = CommandType.Text
                        CommandText = "  UPDATE [UML_CMS].[dbo].[tblgassupplydetails]"
                        CommandText &= " SET [nominatedkwh] = '" & usage_allunit & "'"
                        CommandText &= ", [DMorNDM] = '" & read_frequency & "'"
                        CommandText &= " WHERE [gassupplyid] = " & supplydetailid
                        CO = New SqlCommand(CommandText, myConnection)
                        CO.ExecuteNonQuery()
                    End If
                End If


            End If

        End If
        Dim complete As Boolean = True
        myConnection.Close()

    End Sub

    Protected Sub btnRunImport_Click(sender As Object, e As EventArgs) Handles btnRunImport.Click
        LoadX()
    End Sub
End Class
