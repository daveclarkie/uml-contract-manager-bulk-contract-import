Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Configuration.ConfigurationManager
Imports System.Net
Imports System.Xml

Partial Class helpdesk_recommendationupload
    Inherits System.Web.UI.Page
    Dim strConnection As String = "Data Source=uml-sql\umlsql2005;Initial Catalog=UML_CMS;User Id=sa;Password=chicago;"

    Dim strFile As String = "data\20160726 recommendation helpdesk upload.csv"

    Dim ImportDescription As String = "Bulk Import - Recommendations Bulk Upload"

    Dim strnow As String = Now.Date.ToString("dd MMM yyyy")

    Dim Status As String = ""
    Dim Priority As String = ""
    Dim Technician As String = ""
    Dim Engineer As String = ""
    Dim Customer As String = ""
    Dim SiteInformation As String = ""
    Dim SubProduct As String = ""
    Dim Deliverable As String = ""
    Dim SavingType As String = ""
    Dim FeeType As String = ""
    Dim SharedSavings As String = ""
    Dim ParticipationLength As String = ""
    Dim Year1 As String = ""
    Dim Year2 As String = ""
    Dim Year3 As String = ""
    Dim Year4 As String = ""
    Dim Year5 As String = ""
    Dim Created_Time As String = ""
    Dim ContractID As String = ""
    Dim Group As String = ""
    Dim Commodity As String = ""
    Dim Product As String = ""
    Dim Requester As String = ""
    Dim InvoicingCycle As String = ""
    Dim InvoicingProfile As String = ""
    Dim ServiceStart As Date = Nothing
    Dim ServiceEnd As Date = Nothing
    Dim FirstBillDate As Date = Nothing
    Dim Description As String = ""

    Dim Subject As String = ""
    Dim helpdesk_id As Integer = -1
    Dim isSuccess As Integer = 0

    Dim strXML As String = ""

    Dim mCSV As New CSVData

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.lbloutput.Text = ImportDescription
        Try
            ResetVariables()

        Catch aa As Exception
            System.Diagnostics.Debug.WriteLine(aa.Message.ToString)
        End Try


    End Sub

    Public Sub LoadX()
        '

        ' Set the separator and text qualifier

        mCSV.Separator = ","
        mCSV.TextQualifier = """"
        '
        ' Load in the CSV
        '
        mCSV.LoadCSV(Server.MapPath(strFile), True)
        '
        ' Clear the listview of any data and columns
        '
        Dim maxcol As Integer = mCSV.CSVDataSet.Tables(0).Columns.Count - 1
        Dim dt As DataTable = mCSV.CSVDataSet.Tables(0)
        ProcessBulkDataTable(dt)

    End Sub

    Private Sub ResetVariables()

        Status = ""
        Priority = ""
        Technician = ""
        Engineer = ""
        Customer = ""
        SiteInformation = ""
        SubProduct = ""
        Deliverable = ""
        SavingType = ""
        FeeType = ""
        SharedSavings = ""
        ParticipationLength = ""
        Year1 = ""
        Year2 = ""
        Year3 = ""
        Year4 = ""
        Year5 = ""
        Created_Time = ""
        ContractID = ""
        Group = ""
        Commodity = ""
        Product = ""
        Requester = ""
        InvoicingCycle = ""
        InvoicingProfile = ""
        ServiceStart = Nothing
        ServiceEnd = Nothing
        FirstBillDate = Nothing
        Description = ""
        Subject = ""
        helpdesk_id = -1
        isSuccess = 0
        strXML = ""


    End Sub

    Private Function getfirst(ByVal str As String) As String
        Dim out As String = ""

        Dim chrPos As Integer = 0
        Dim chrMax As Integer = str.Length

        Do While chrPos < chrMax
            If chrPos > 0 Then
                If Char.IsUpper(str, chrPos) Then
                    If Char.IsLower(str, chrPos - 1) Then
                        Exit Do
                    End If
                    If str(chrPos - 1).ToString = ")" Then
                        Exit Do
                    End If
                    If Char.IsDigit(str, chrPos - 1) Then
                        If Not Char.IsWhiteSpace(str, chrPos + 1) Then
                            Exit Do
                        End If
                    End If
                End If
                If Char.IsDigit(str, chrPos) Then
                    If Char.IsLower(str, chrPos - 1) Then
                        Exit Do
                    End If
                End If
                If str(chrPos).ToString = "," Then
                    Exit Do
                End If
            End If
            out += str(chrPos).ToString
            chrPos += 1
        Loop
        Return out
    End Function

    Private Sub ProcessBulkDataTable(ByVal dt As DataTable)

        System.Diagnostics.Debug.WriteLine(ImportDescription)

        Dim dr As DataRow
        Dim iRowIndex As Integer = 0
        For Each dr In dt.Rows
            iRowIndex += 1
            ResetVariables()

            If Not IsDBNull(dr(1)) Then

                Status = dr("Status").ToString.Replace("'", "")
                Priority = dr("Priority").ToString.Replace("'", "")
                Technician = dr("Technician").ToString.Replace("'", "")
                Engineer = dr("Engineer").ToString.Replace("'", "")
                Customer = dr("Customer").ToString.Replace("'", "")
                SiteInformation = dr("Site Information").ToString.Replace("'", "")
                SubProduct = dr("Sub Product").ToString.Replace("'", "")
                Deliverable = dr("Deliverable").ToString.Replace("'", "")
                SavingType = dr("Saving Type").ToString.Replace("'", "")
                FeeType = dr("Fee Type").ToString.Replace("'", "")
                SharedSavings = dr("Shared Savings").ToString.Replace("'", "")
                ParticipationLength = dr("Participation Length").ToString.Replace("'", "")
                Year1 = dr("Year 1").ToString.Replace("'", "")
                Year2 = dr("Year 2").ToString.Replace("'", "")
                Year3 = dr("Year 3").ToString.Replace("'", "")
                Year4 = dr("Year 4").ToString.Replace("'", "")
                Year5 = dr("Year 5").ToString.Replace("'", "")
                ContractID = dr("Contract ID").ToString.Replace("'", "")
                Group = dr("Group").ToString.Replace("'", "")
                Commodity = dr("Commodity").ToString.Replace("'", "")
                Product = dr("Product").ToString.Replace("'", "")
                Requester = dr("Requester").ToString.Replace("'", "")
                InvoicingCycle = dr("Invoicing Cycle").ToString.Replace("'", "")
                InvoicingProfile = dr("Invoicing Profile").ToString.Replace("'", "")

                If Not dr("Service Start") = "" Then ServiceStart = CDate(dr("Service Start"))
                If Not dr("Service End") = "" Then ServiceEnd = CDate(dr("Service End"))
                If Not dr("First Bill Date") = "" Then FirstBillDate = CDate(dr("First Bill Date"))

                Description = dr("Description").ToString.Replace("'", "")
                Subject = "Recommendation - " + SiteInformation + " | " + Commodity + " | " + SubProduct + " | " + Deliverable
                'Requestor = "Dave Clarke Request"
                'Technician = "Dave Clarke"

                SetVariables()

                System.Diagnostics.Debug.WriteLine(iRowIndex & " / " & dt.Rows.Count & " (" & (iRowIndex / dt.Rows.Count) * 100 & " %)")
                Me.lbloutput.Text &= "<br /><b>" & iRowIndex & " / " & dt.Rows.Count & "</b> (" & (iRowIndex / dt.Rows.Count) * 100 & " %) | <b>Success:</b> " & isSuccess & " | <b>Subject:</b> " & Subject

            End If
        Next

    End Sub

    Private Sub SetVariables()

        strXML = "?OPERATION_NAME=ADD_REQUEST&TECHNICIAN_KEY=" & AppSettings("RestAPIKey") & "&INPUT_DATA="
        strXML &= "<Operation>"
        strXML &= "<Details>"

        strXML &= "<parameter>"
        strXML &= "<name>requesttemplate</name>"
        strXML &= "<value>Water Recommendation</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>status</name>"
        strXML &= "<value>" & Status & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>priority</name>"
        strXML &= "<value>" & Priority & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>requester</name>"
        strXML &= "<value>" & Requester & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Subject</name>"
        strXML &= "<value>" & Subject & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Customer</name>"
        strXML &= "<value>" & Customer & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Deliverable</name>"
        strXML &= "<value>" & Deliverable & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Fee Type</name>"
        strXML &= "<value>" & FeeType & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Invoicing Profile</name>"
        strXML &= "<value>" & InvoicingProfile & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Engineer</name>"
        strXML &= "<value>" & Engineer & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Site Information</name>"
        strXML &= "<value>" & SiteInformation & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Commodity</name>"
        strXML &= "<value>" & Commodity & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Invoicing Cycle</name>"
        strXML &= "<value>" & InvoicingCycle & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Sub Product</name>"
        strXML &= "<value>" & SubProduct & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Saving Type</name>"
        strXML &= "<value>" & SavingType & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Contract ID</name>"
        strXML &= "<value>" & ContractID & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Product</name>"
        strXML &= "<value>" & Product & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Shared Savings %</name>"
        strXML &= "<value>" & SharedSavings & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Participation Length</name>"
        strXML &= "<value>" & ParticipationLength & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Year 1</name>"
        strXML &= "<value>" & Year1 & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Year 2</name>"
        strXML &= "<value>" & Year2 & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Year 3</name>"
        strXML &= "<value>" & Year3 & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Year 4</name>"
        strXML &= "<value>" & Year4 & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Year 5</name>"
        strXML &= "<value>" & Year5 & "</value>"
        strXML &= "</parameter>"

        If Not IsDBNull(ServiceStart) Then

            Dim date1 As Date = ServiceStart
            Dim date1_string As String = date1.ToString("dd MMM yyyy, HH:mm:ss")
            
            strXML &= "<parameter>"
            strXML &= "<name>Service Start</name>"
            strXML &= "<value>" & date1_string & "</value>"
            strXML &= "</parameter>"

        End If

        If Not IsDBNull(ServiceEnd) Then

            Dim date2 As Date = ServiceEnd
            Dim date2_string As String = date2.ToString("dd MMM yyyy, HH:mm:ss")

            strXML &= "<parameter>"
            strXML &= "<name>Service End</name>"
            strXML &= "<value>" & date2_string & "</value>"
            strXML &= "</parameter>"

        End If

        If Not IsDBNull(FirstBillDate) Then

            Dim date3 As Date = FirstBillDate
            Dim date3_string As String = date3.ToString("dd MMM yyyy, HH:mm:ss")

            strXML &= "<parameter>"
            strXML &= "<name>First Bill Date</name>"
            strXML &= "<value>" & date3_string & "</value>"
            strXML &= "</parameter>"

        End If

        strXML &= "<parameter>"
        strXML &= "<name>Technician</name>"
        strXML &= "<value>" & Technician & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Description</name>"
        strXML &= "<value>" & Description & "</value>"
        strXML &= "</parameter>"

        strXML &= "</Details>"
        strXML &= "</Operation>"

        'strXML &= "<parameter>"
        'strXML &= "<name></name>"
        'strXML &= "<value></value>"
        'strXML &= "</parameter>"

        'System.Diagnostics.Debug.WriteLine(strXML)
        Dim strHD As String = AppSettings("HelpdeskURL") & AppSettings("APIURL")

        Dim outcome As String = PostTo(strHD, strXML)

        If outcome.StartsWith("Success: ") Then
            isSuccess = 1
        End If
    End Sub

    Protected Function PostTo(url As String, postData As String) As String
        Dim myWebRequest As HttpWebRequest = TryCast(WebRequest.Create(url & postData), HttpWebRequest)
        myWebRequest.Method = "POST"
        myWebRequest.ContentType = "application/x-www-form-urlencoded"
        Dim dataStream As System.IO.Stream = myWebRequest.GetRequestStream()
        dataStream.Close()

        Dim myWebResponse As WebResponse = myWebRequest.GetResponse()
        dataStream = myWebResponse.GetResponseStream()
        Dim reader As New System.IO.StreamReader(dataStream)
        Dim responseFromServer As String = reader.ReadToEnd()

        Dim xmlDoc As New XmlDocument
        xmlDoc.LoadXml(responseFromServer)

        Dim formResponse As String = ""
        Try
            Dim outcome As String = xmlDoc.SelectSingleNode("/API/response/operation/result/status").InnerText
            If outcome = "Success" Then
                formResponse = "Success: Request ID " & xmlDoc.SelectSingleNode("/API/response/operation/Details/workorderid").InnerText
            ElseIf outcome = "Failure" Then
                Dim xmlReason As String = xmlDoc.SelectSingleNode("/API/response/operation/result/message").InnerText
                formResponse = "Error: " & xmlReason & "</td></tr>"
            End If
        Catch ex As Exception

        End Try
        reader.Close()
        dataStream.Close()
        myWebResponse.Close()

        Return formResponse

    End Function

    Protected Sub btnRunImport_Click(sender As Object, e As EventArgs) Handles btnRunImport.Click
        LoadX()
    End Sub

    Public Shared Function fnHelpdeskURL() As String
        Dim CO As New SqlCommand
        Dim CommandText As String
        Dim myConnection As New SqlConnection("")

        myConnection.Open()
        CO.CommandType = CommandType.Text
        CommandText = "EXEC uml_cms.dbo.rsp_configuration_helpdeskurl_latest"
        CO = New SqlCommand(CommandText, myConnection)

        Dim _return As String = CO.ExecuteScalar
        Return _return
    End Function

    Public Shared Function fnHelpdeskRESTAPIURL() As String
        Dim CO As New SqlCommand
        Dim CommandText As String
        Dim myConnection As New SqlConnection("")

        myConnection.Open()
        CO.CommandType = CommandType.Text
        CommandText = "EXEC uml_cms.dbo.rsp_configuration_helpdeskrestapiurl_latest"
        CO = New SqlCommand(CommandText, myConnection)

        Dim _return As String = CO.ExecuteScalar
        Return _return
    End Function

    Public Shared Function fnHelpdeskRESTAPIkey() As String
        Dim CO As New SqlCommand
        Dim CommandText As String
        Dim myConnection As New SqlConnection("")

        myConnection.Open()
        CO.CommandType = CommandType.Text
        CommandText = "EXEC uml_cms.dbo.rsp_configuration_helpdeskrestapikey_latest"
        CO = New SqlCommand(CommandText, myConnection)

        Dim _return As String = CO.ExecuteScalar
        Return _return
    End Function

    Public Shared Function fnSMTPServer() As String
        Dim CO As New SqlCommand
        Dim CommandText As String
        Dim myConnection As New SqlConnection("")

        myConnection.Open()
        CO.CommandType = CommandType.Text
        CommandText = "EXEC uml_cms.dbo.rsp_configuration_smtpserver_latest"
        CO = New SqlCommand(CommandText, myConnection)

        Dim _return As String = CO.ExecuteScalar
        Return _return
    End Function

End Class
