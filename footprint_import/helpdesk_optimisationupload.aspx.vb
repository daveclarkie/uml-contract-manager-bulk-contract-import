Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Configuration.ConfigurationManager
Imports System.Net
Imports System.Xml

Partial Class helpdesk_optimisationupload
    Inherits System.Web.UI.Page
    Dim strConnection As String = "Data Source=uml-sql\umlsql2005;Initial Catalog=UML_CMS;User Id=sa;Password=chicago;"

    Dim strFile As String = "data\20151007 optimisation helpdesk upload.csv"

    Dim ImportDescription As String = "Bulk Import - Regional UK Bulk Upload"

    Dim strnow As String = Now.Date.ToString("dd MMM yyyy")

    Dim Requestor As String = ""
    Dim Technician As String = ""
    Dim Subject As String = ""
    Dim Description As String = ""

    Dim Customer As String = ""
    Dim Country As String = ""

    Dim SiteName As String = ""
    Dim MeterNumber As String = ""
    Dim NameofCM As String = ""
    Dim NameofRecommender As String = ""

    Dim Comments As String = ""
    Dim Actions As String = ""

    Dim TotalAnnualExpectedGrossSaving As Decimal = "0.00"
    Dim RevenueValue As Decimal = "0.00"

    Dim helpdesk_id As Integer = -1
    Dim isSuccess As Integer = 0

    Dim strXML As String = ""

    Dim mCSV As New CSVData

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.lbloutput.Text = ImportDescription
        Try
            ResetVariables()

        Catch aa As Exception
            System.Diagnostics.Debug.WriteLine(aa.Message.ToString)
        End Try


    End Sub

    Public Sub LoadX()
        '

        ' Set the separator and text qualifier

        mCSV.Separator = ","
        mCSV.TextQualifier = """"
        '
        ' Load in the CSV
        '
        mCSV.LoadCSV(Server.MapPath(strFile), True)
        '
        ' Clear the listview of any data and columns
        '
        Dim maxcol As Integer = mCSV.CSVDataSet.Tables(0).Columns.Count - 1
        Dim dt As DataTable = mCSV.CSVDataSet.Tables(0)
        ProcessBulkDataTable(dt)

    End Sub

    Private Sub ResetVariables()

        Requestor = ""
        Technician = ""
        Subject = ""
        Description = ""

        Customer = ""
        Country = ""

        SiteName = ""
        MeterNumber = ""
        NameofCM = ""
        NameofRecommender = ""

        Comments = ""
        Actions = ""

        TotalAnnualExpectedGrossSaving = "0.00"
        RevenueValue = "0.00"

        helpdesk_id = -1
        isSuccess = 0
        strXML = ""


    End Sub

    Private Function getfirst(ByVal str As String) As String
        Dim out As String = ""

        Dim chrPos As Integer = 0
        Dim chrMax As Integer = str.Length

        Do While chrPos < chrMax
            If chrPos > 0 Then
                If Char.IsUpper(str, chrPos) Then
                    If Char.IsLower(str, chrPos - 1) Then
                        Exit Do
                    End If
                    If str(chrPos - 1).ToString = ")" Then
                        Exit Do
                    End If
                    If Char.IsDigit(str, chrPos - 1) Then
                        If Not Char.IsWhiteSpace(str, chrPos + 1) Then
                            Exit Do
                        End If
                    End If
                End If
                If Char.IsDigit(str, chrPos) Then
                    If Char.IsLower(str, chrPos - 1) Then
                        Exit Do
                    End If
                End If
                If str(chrPos).ToString = "," Then
                    Exit Do
                End If
            End If
            out += str(chrPos).ToString
            chrPos += 1
        Loop
        Return out
    End Function

    Private Sub ProcessBulkDataTable(ByVal dt As DataTable)

        System.Diagnostics.Debug.WriteLine(ImportDescription)

        Dim dr As DataRow
        Dim iRowIndex As Integer = 0
        For Each dr In dt.Rows
            iRowIndex += 1
            ResetVariables()

            If Not IsDBNull(dr(1)) Then

                Requestor = dr("REQUESTOR").ToString.Replace("'", "")
                Technician = dr("TECHNICIAN").ToString.Replace("'", "")
                Subject = dr("Subject Main").ToString.Replace("'", "")
                Description = dr("Description").ToString.Replace("'", "")

                Customer = dr("CUSTOMER").ToString.Replace("'", "")
                Country = dr("Country").ToString.Replace("'", "")

                SiteName = dr("SITE NAME").ToString.Replace("'", "")
                MeterNumber = dr("METER NUMBER").ToString.Replace("'", "")
                NameofCM = dr("CLIENT MANAGER").ToString.Replace("'", "")
                NameofRecommender = dr("RECOMMENDOR").ToString.Replace("'", "")

                Comments = dr("COMMENTS").ToString.Replace("'", "")
                Actions = dr("ACTIONS").ToString.Replace("'", "")

                TotalAnnualExpectedGrossSaving = dr("TOTAL ANNUAL GROSS SAVING").ToString.Replace("'", "")
                If dr("REVENUE Anticipated").ToString.Replace("'", "") = "" Then
                    RevenueValue = 0
                ElseIf IsNumeric(dr("REVENUE Anticipated").ToString.Replace("'", "")) = False Then
                    RevenueValue = 0
                Else
                    RevenueValue = dr("REVENUE Anticipated").ToString.Replace("'", "")
                End If

                'Requestor = "Dave Clarke Request"
                'Technician = "Dave Clarke"

                SetVariables()

                System.Diagnostics.Debug.WriteLine(iRowIndex & " / " & dt.Rows.Count & " (" & (iRowIndex / dt.Rows.Count) * 100 & " %)")
                Me.lbloutput.Text &= "<br /><b>" & iRowIndex & " / " & dt.Rows.Count & "</b> (" & (iRowIndex / dt.Rows.Count) * 100 & " %) | <b>Success:</b> " & isSuccess & " | <b>Subject:</b> " & Subject

            End If
        Next

    End Sub

    Private Sub SetVariables()

        strXML = "?OPERATION_NAME=ADD_REQUEST&TECHNICIAN_KEY=" & AppSettings("RestAPIKey") & "&INPUT_DATA="
        strXML &= "<Operation>"
        strXML &= "<Details>"

        strXML &= "<parameter>"
        strXML &= "<name>requesttemplate</name>"
        strXML &= "<value>Regional, UK Optimisation</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>requester</name>"
        strXML &= "<value>" & Requestor & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Subject</name>"
        strXML &= "<value>" & Subject & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Customer</name>"
        strXML &= "<value>" & Customer & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Country</name>"
        strXML &= "<value>" & Country & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Site Name</name>"
        strXML &= "<value>" & SiteName & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Meter Number</name>"
        strXML &= "<value>" & MeterNumber & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Name of CM</name>"
        strXML &= "<value>" & NameofCM & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Name of Recommender</name>"
        strXML &= "<value>" & NameofRecommender & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Total Annual Expected Gross Saving</name>"
        strXML &= "<value>" & TotalAnnualExpectedGrossSaving & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Revenue Value</name>"
        strXML &= "<value>" & RevenueValue & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Technician</name>"
        strXML &= "<value>" & Technician & "</value>"
        strXML &= "</parameter>"

        strXML &= "<parameter>"
        strXML &= "<name>Description</name>"
        strXML &= "<value>" & Description & "</value>"
        strXML &= "</parameter>"

        strXML &= "</Details>"
        strXML &= "</Operation>"

        'strXML &= "<parameter>"
        'strXML &= "<name></name>"
        'strXML &= "<value></value>"
        'strXML &= "</parameter>"

        'System.Diagnostics.Debug.WriteLine(strXML)
        Dim strHD As String = AppSettings("HelpdeskURL") & AppSettings("APIURL")

        Dim outcome As String = PostTo(strHD, strXML)

        If outcome.StartsWith("Success: ") Then
            isSuccess = 1
        End If
    End Sub

    Protected Function PostTo(url As String, postData As String) As String
        Dim myWebRequest As HttpWebRequest = TryCast(WebRequest.Create(url & postData), HttpWebRequest)
        myWebRequest.Method = "POST"
        myWebRequest.ContentType = "application/x-www-form-urlencoded"
        Dim dataStream As System.IO.Stream = myWebRequest.GetRequestStream()
        dataStream.Close()

        Dim myWebResponse As WebResponse = myWebRequest.GetResponse()
        dataStream = myWebResponse.GetResponseStream()
        Dim reader As New System.IO.StreamReader(dataStream)
        Dim responseFromServer As String = reader.ReadToEnd()

        Dim xmlDoc As New XmlDocument
        xmlDoc.LoadXml(responseFromServer)

        Dim formResponse As String = ""
        Try
            Dim outcome As String = xmlDoc.SelectSingleNode("/API/response/operation/result/status").InnerText
            If outcome = "Success" Then
                formResponse = "Success: Request ID " & xmlDoc.SelectSingleNode("/API/response/operation/Details/workorderid").InnerText
            ElseIf outcome = "Failure" Then
                Dim xmlReason As String = xmlDoc.SelectSingleNode("/API/response/operation/result/message").InnerText
                formResponse = "Error: " & xmlReason & "</td></tr>"
            End If
        Catch ex As Exception

        End Try
        reader.Close()
        dataStream.Close()
        myWebResponse.Close()

        Return formResponse

    End Function

    Protected Sub btnRunImport_Click(sender As Object, e As EventArgs) Handles btnRunImport.Click
        LoadX()
    End Sub

    Public Shared Function fnHelpdeskURL() As String
        Dim CO As New SqlCommand
        Dim CommandText As String
        Dim myConnection As New SqlConnection("")

        myConnection.Open()
        CO.CommandType = CommandType.Text
        CommandText = "EXEC uml_cms.dbo.rsp_configuration_helpdeskurl_latest"
        CO = New SqlCommand(CommandText, myConnection)

        Dim _return As String = CO.ExecuteScalar
        Return _return
    End Function

    Public Shared Function fnHelpdeskRESTAPIURL() As String
        Dim CO As New SqlCommand
        Dim CommandText As String
        Dim myConnection As New SqlConnection("")

        myConnection.Open()
        CO.CommandType = CommandType.Text
        CommandText = "EXEC uml_cms.dbo.rsp_configuration_helpdeskrestapiurl_latest"
        CO = New SqlCommand(CommandText, myConnection)

        Dim _return As String = CO.ExecuteScalar
        Return _return
    End Function

    Public Shared Function fnHelpdeskRESTAPIkey() As String
        Dim CO As New SqlCommand
        Dim CommandText As String
        Dim myConnection As New SqlConnection("")

        myConnection.Open()
        CO.CommandType = CommandType.Text
        CommandText = "EXEC uml_cms.dbo.rsp_configuration_helpdeskrestapikey_latest"
        CO = New SqlCommand(CommandText, myConnection)

        Dim _return As String = CO.ExecuteScalar
        Return _return
    End Function

    Public Shared Function fnSMTPServer() As String
        Dim CO As New SqlCommand
        Dim CommandText As String
        Dim myConnection As New SqlConnection("")

        myConnection.Open()
        CO.CommandType = CommandType.Text
        CommandText = "EXEC uml_cms.dbo.rsp_configuration_smtpserver_latest"
        CO = New SqlCommand(CommandText, myConnection)

        Dim _return As String = CO.ExecuteScalar
        Return _return
    End Function

End Class
