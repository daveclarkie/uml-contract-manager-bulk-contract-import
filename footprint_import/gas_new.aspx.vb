Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class _gas_new
    Inherits System.Web.UI.Page

    Dim strFile As String = "data\53876 Gas New Meter.csv"
    Dim strConnection As String = "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=UML_CMS;Trusted_Connection=True;"
    Dim CommandText As String = ""
    Dim myConnection As SqlConnection
    Dim CO As SqlCommand

    Dim ImportDescription As String = Left("Bulk Import - " & strFile.ToString.Replace("data\", ""), 50)

    Dim strnow As String = Now.Date.ToString("dd MMM yyyy")

    Dim customer_id As String = ""
    Dim customer_name As String = ""

    Dim site_id As String = ""
    Dim site_name As String = ""

    Dim analyst_name As String = ""
    Dim accountmananger_name As String = ""

    Dim fuel As String = ""
    Dim start_date As String = ""
    Dim end_date As String = ""
    Dim meter As String = ""
    Dim read_frequency As String = ""

    Dim supplier_name As String = ""
    Dim address_1 As String = ""
    Dim address_2 As String = ""
    Dim address_3 As String = ""
    Dim postcode As String = ""
    Dim country As String = ""

    Dim rate_allunit As String = ""
    Dim rate_day As String = ""
    Dim rate_night As String = ""

    Dim usage_allunit As String = ""
    Dim usage_day As String = ""
    Dim usage_night As String = ""

    Dim standingcharge_month As String = ""
    Dim commission_kwh As String = ""
    Dim commission_year As String = ""

    Dim contract_type As String = ""
    Dim tender_status As String = ""
    Dim termination_optout As String = ""
    Dim registration_confirmed As String = ""


    Dim tenderid As Integer = -1
    Dim rateid As Integer = -1
    Dim contractid As Integer = -1
    Dim supplydetailid As Integer = -1


    Dim mCSV As New CSVData

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.lbloutput.Text = ImportDescription
        Try
            ResetVariables()

            If lblBeforeCustomers.Text = 0 Then
                CommandText = "SELECT count(*) FROM tblCustomer"
                myConnection = New SqlConnection(strConnection)
                CO = New SqlCommand(CommandText, myConnection)
                myConnection.Open()
                lblBeforeCustomers.Text = Convert.ToString(CO.ExecuteScalar())
            End If

        Catch aa As Exception
            System.Diagnostics.Debug.WriteLine(aa.Message.ToString)
        End Try


    End Sub

    Public Sub LoadX()
        '

        ' Set the separator and text qualifier

        mCSV.Separator = ","
        mCSV.TextQualifier = """"
        '
        ' Load in the CSV
        '
        mCSV.LoadCSV(Server.MapPath(strFile), True)
        '
        ' Clear the listview of any data and columns
        '
        Dim maxcol As Integer = mCSV.CSVDataSet.Tables(0).Columns.Count - 1
        Dim dt As DataTable = mCSV.CSVDataSet.Tables(0)
        ProcessBulkDataTable(dt)

    End Sub

    Private Sub ResetVariables()

        customer_id = ""
        customer_name = ""

        site_id = ""
        site_name = ""

        analyst_name = ""
        accountmananger_name = ""

        fuel = ""
        start_date = ""
        end_date = ""
        meter = ""
        read_frequency = ""

        supplier_name = ""
        address_1 = ""
        address_2 = ""
        address_3 = ""
        postcode = ""
        country = ""

        rate_allunit = ""
        rate_day = ""
        rate_night = ""

        usage_allunit = ""
        usage_day = ""
        usage_night = ""

        standingcharge_month = ""
        commission_kwh = ""
        commission_year = ""

        contract_type = ""
        tender_status = ""
        termination_optout = ""
        registration_confirmed = ""

        tenderid = -1
        rateid = -1
        contractid = -1
        supplydetailid = -1

    End Sub

    Private Function getfirst(ByVal str As String) As String
        Dim out As String = ""

        Dim chrPos As Integer = 0
        Dim chrMax As Integer = str.Length

        Do While chrPos < chrMax
            If chrPos > 0 Then
                If Char.IsUpper(str, chrPos) Then
                    If Char.IsLower(str, chrPos - 1) Then
                        Exit Do
                    End If
                    If str(chrPos - 1).ToString = ")" Then
                        Exit Do
                    End If
                    If Char.IsDigit(str, chrPos - 1) Then
                        If Not Char.IsWhiteSpace(str, chrPos + 1) Then
                            Exit Do
                        End If
                    End If
                End If
                If Char.IsDigit(str, chrPos) Then
                    If Char.IsLower(str, chrPos - 1) Then
                        Exit Do
                    End If
                End If
                If str(chrPos).ToString = "," Then
                    Exit Do
                End If
            End If
            out += str(chrPos).ToString
            chrPos += 1
        Loop
        Return out
    End Function

    Private Sub ProcessBulkDataTable(ByVal dt As DataTable)

        Dim dr As DataRow
        Dim iRowIndex As Integer = 0
        For Each dr In dt.Rows
            iRowIndex += 1
            ResetVariables()

            customer_id = dr("customer_id").ToString.Replace("'", "")
            customer_name = dr("customer_name").ToString.Replace("'", "")

            site_id = dr("site_id").ToString.Replace("'", "")
            site_name = dr("site_name").ToString.Replace("'", "")

            analyst_name = dr("analyst_name").ToString.Replace("'", "")
            accountmananger_name = dr("accountmanager_name").ToString.Replace("'", "")

            fuel = dr("fuel").ToString.Replace("'", "")
            start_date = dr("start_date").ToString.Replace("'", "")
            end_date = dr("end_date").ToString.Replace("'", "")
            meter = dr("meter").ToString.Replace("'", "")
            read_frequency = dr("read_frequency").ToString.Replace("'", "")

            supplier_name = dr("supplier_name").ToString.Replace("'", "")
            address_1 = dr("address_1").ToString.Replace("'", "")
            address_2 = dr("address_2").ToString.Replace("'", "")
            address_3 = dr("address_3").ToString.Replace("'", "")
            'county = dr("county").ToString.Replace("'", "")
            postcode = dr("postcode").ToString.Replace("'", "")

            Try
                country = dr("country").ToString.Replace("'", "")
            Catch ex As Exception
                country = ""
            End Try
            If country = "" Then : country = "United Kingdom" : End If


            rate_allunit = dr("rate_allunit").ToString.Replace("'", "")
            rate_day = dr("rate_day").ToString.Replace("'", "")
            rate_night = dr("rate_night").ToString.Replace("'", "")

            usage_allunit = dr("usage_allunit").ToString.Replace("'", "")
            usage_day = dr("usage_day").ToString.Replace("'", "")
            usage_night = dr("usage_night").ToString.Replace("'", "")

            standingcharge_month = dr("standingcharge_month").ToString.Replace("'", "")
            commission_kwh = dr("commission_kwh").ToString.Replace("'", "")
            commission_year = dr("commission_year").ToString.Replace("'", "")

            contract_type = dr("contract_type").ToString.Replace("'", "")
            tender_status = dr("tender_status").ToString.Replace("'", "")
            termination_optout = dr("termination_optout").ToString.Replace("'", "")
            registration_confirmed = dr("registration_confirmed").ToString.Replace("'", "")

            SetVariables()
            If tenderid = -1 Then
                'System.Diagnostics.Debug.WriteLine(iRowIndex & " | " & customer_name & " | " & site_name & " | tender: " & tenderid & " | history: " & contractid & " (" & iRowIndex & "/" & dt.Rows.Count & ")")
                SetVariables()
                'System.Diagnostics.Debug.WriteLine(iRowIndex & " | " & customer_name & " | " & site_name & " | tender: " & tenderid & " | history: " & contractid & " (" & iRowIndex & "/" & dt.Rows.Count & ")")
            End If

            System.Diagnostics.Debug.WriteLine(iRowIndex & " / " & dt.Rows.Count & " (" & (iRowIndex / dt.Rows.Count) * 100 & " %)")
            Me.lbloutput.Text &= "<br />" & iRowIndex & " | " & customer_name & " | " & site_name & " | tender: " & tenderid & " | history: " & contractid & " (" & iRowIndex & "/" & dt.Rows.Count & ")"

        Next

        CommandText = "SELECT count(*) FROM tblCustomer"
        myConnection = New SqlConnection(strConnection)
        CO = New SqlCommand(CommandText, myConnection)
        myConnection.Open()
        lblAfterCustomers.Text = Convert.ToString(CO.ExecuteScalar())

    End Sub

    Private Sub SetVariables()


        myConnection = New SqlConnection(strConnection)
        myConnection.Open()
        CO = New SqlCommand

        If site_name = "N/A" Then
            site_name = address_1 & " - " & Right(meter, 3)
        End If
        If site_name.Contains("'") Then
            site_name = site_name.Replace("'", "")
        End If


        Dim custid As Integer = -1
        Dim siteid As Integer = -1
        Dim analystid As Integer = -1
        Dim accountmanangerid As Integer = -1
        Dim supplierid As Integer = -1
        Dim contractypeid As Integer = -1

        'analyst
        CO.CommandType = CommandType.Text
        CommandText = "SELECT AccmgrId FROM dbo.tblaccmgr WHERE Accountmgr = '" & analyst_name & "'"
        CO = New SqlCommand(CommandText, myConnection)
        analystid = CInt(CO.ExecuteScalar)
        If Not analystid > 0 Then
            analystid = -1
        End If

        'account mananger
        CO.CommandType = CommandType.Text
        CommandText = "SELECT AccmgrId FROM dbo.tblaccmgr WHERE Accountmgr = '" & accountmananger_name & "'"
        CO = New SqlCommand(CommandText, myConnection)
        accountmanangerid = CInt(CO.ExecuteScalar)
        If Not accountmanangerid > 0 Then
            accountmanangerid = -1
        End If

        'supplier name
        CO.CommandType = CommandType.Text
        CommandText = "SELECT SupplierId FROM [UML_CMS].[dbo].[tblsuppliers] WHERE Supplier_name = '" & supplier_name & "'"
        CO = New SqlCommand(CommandText, myConnection)
        supplierid = CInt(CO.ExecuteScalar)
        If Not supplierid > 0 Then
            supplierid = -1
        End If

        'contract type
        CO.CommandType = CommandType.Text
        CommandText = "SELECT ID FROM dbo.tbl_Purchasing_BasketAdministration WHERE iType = 'G' AND BasketDescription = '" & contract_type & "'"
        CO = New SqlCommand(CommandText, myConnection)
        contractypeid = CInt(CO.ExecuteScalar)
        If Not contractypeid > 0 Then
            contractypeid = -1
        End If

        'tender status
        CO.CommandType = CommandType.Text
        CommandText = "SELECT ID FROM dbo.tbl_Purchasing_BasketAdministration WHERE iType = 'G' AND BasketDescription = '" & contract_type & "'"
        CO = New SqlCommand(CommandText, myConnection)
        contractypeid = CInt(CO.ExecuteScalar)
        If Not contractypeid > 0 Then
            contractypeid = -1
        End If

        ' try and match customers
        If customer_id = "" Or customer_id = "N/A" Then
            CO.CommandType = CommandType.Text
            CommandText = "SELECT custid FROM tblCustomer WHERE customername = '" & customer_name & "'"
            CO = New SqlCommand(CommandText, myConnection)
            custid = CInt(CO.ExecuteScalar())
        Else
            custid = customer_id
        End If

        If custid < 1 Then ' if there is no customer then create one
            CommandText = " INSERT INTO tblCustomer (customername, address_1, address_2, address_3, pcode, custorprospect, prodgasneg) VALUES ('" & Trim(customer_name) & "','" & Trim(Left(address_1, 30)) & "','" & Trim(address_2) & "','" & Trim(address_3) & "','" & Trim(postcode) & "', 2, -1);"
            CommandText &= "SELECT @@IDENTITY;"
            CO = New SqlCommand(CommandText, myConnection)
            custid = CInt(CO.ExecuteScalar)
        End If

        Dim tenderproductid As Integer = contractypeid

        ' try and match sites
        If site_id = "" Or site_id = "N/A" Then
            CO.CommandType = CommandType.Text
            CommandText = "SELECT TOP 1 siteid FROM tblSites WHERE sitename = '" & site_name & "' AND custid = " & custid
            CO = New SqlCommand(CommandText, myConnection)
            siteid = CInt(CO.ExecuteScalar)
        Else
            siteid = site_id
        End If


        ' get the country FK
        Dim country_pk As Integer = -1
        CO.CommandType = CommandType.Text
        CommandText = "SELECT TOP 1 intCountryPK FROM lkpCountry WHERE countrydesc = '" & country & "' OR printablename = '" & country & "'"
        CO = New SqlCommand(CommandText, myConnection)
        country_pk = CInt(CO.ExecuteScalar)


        If siteid < 1 Then ' if there is no site then create one
            CO.CommandType = CommandType.Text
            CommandText = "INSERT INTO [UML_CMS].[dbo].[tblsites] ([custid],[sitename],[deadoralive],[Address_1],[Address_2],[Address_3],[Pcode],[prodelecneg],[prodgasneg],[statusid], [intCountryFK])"
            CommandText &= " VALUES (" & custid & ", '" & Trim(site_name) & "', 0,'" & Trim(Left(address_1, 49)) & "','" & Trim(Left(address_2, 49)) & "','" & Trim(Left(address_3, 49)) & "','" & Trim(postcode) & "', 0, -1, 4, " & country_pk & ");"
            CommandText &= " SELECT @@IDENTITY;"
            CO = New SqlCommand(CommandText, myConnection)
            siteid = CInt(CO.ExecuteScalar)
        End If

        'update account manager if provided
        If accountmanangerid > 0 Then
            CO = New SqlCommand
            CO.CommandType = CommandType.Text
            CommandText = "UPDATE tblcustomer SET FKaccmgrid = " & accountmanangerid & " WHERE Custid = " & custid
            CO = New SqlCommand(CommandText, myConnection)
            CO.ExecuteNonQuery()
        End If

        Dim siteexists As Integer = -1
        CO = New SqlCommand
        CO.CommandType = CommandType.Text
        CommandText = "SELECT CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END FROM dbo.tblsites WHERE siteid = " & siteid
        CO = New SqlCommand(CommandText, myConnection)
        siteexists = CInt(CO.ExecuteScalar)

        If siteexists = 0 Then
            'System.Diagnostics.Debug.WriteLine("Site Exist: " & CStr(siteexists) & " | " & siteid)
        Else
            ' sets tender complete
            CO = New SqlCommand
            CO.CommandType = CommandType.Text
            CommandText = "UPDATE tblsites SET statusid = 4 WHERE siteid = " & siteid
            CO = New SqlCommand(CommandText, myConnection)
            CO.ExecuteNonQuery()

            ' updates analyst
            If analystid > 0 Then
                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "UPDATE tblsites SET Accmgrid = " & analystid & " WHERE siteid = " & siteid
                CO = New SqlCommand(CommandText, myConnection)
                CO.ExecuteNonQuery()
            End If

            CO = New SqlCommand
            CO.CommandType = CommandType.Text
            CommandText = "SELECT CASE WHEN LEN(ISNULL(Address_1,'')) = 0 THEN 1 ELSE 0 END from tblsites where siteid = " & siteid
            CO = New SqlCommand(CommandText, myConnection)
            Dim addressupdate As Integer = -1
            addressupdate = CInt(CO.ExecuteScalar)

            If addressupdate = 1 Then
                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "UPDATE tblSites SET address_1 = '" & address_1 & "', address_2 = '" & address_2 & "', address_3 = '" & address_3 & "', pcode = '" & postcode & "'  where siteid = " & siteid
                CO = New SqlCommand(CommandText, myConnection)
                CO.ExecuteNonQuery()
            End If

            '   check to see if meter is already attached to site?
            CO = New SqlCommand
            CO.CommandType = CommandType.Text
            CommandText = "SELECT mnumid FROM tblmnumbers where mnumber like '" & meter & "'"
            Dim meterid As Integer = -1
            CO = New SqlCommand(CommandText, myConnection)
            meterid = CO.ExecuteScalar

            If meterid < 1 And meter.Length > 0 Then
                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "INSERT INTO [UML_CMS].[dbo].[tblmnumbers] ([siteID],[Mnumber],[CRC_Required]) VALUES (" & siteid & ",'" & meter & "',0);"
                CommandText &= "SELECT @@IDENTITY;"
                CO = New SqlCommand(CommandText, myConnection)
                meterid = CInt(CO.ExecuteScalar)
            End If
            
            ' contract stuff

            Select Case supplierid
                Case Nothing
                    supplierid = 42
            End Select

            CommandText = "SELECT PKgasratehistid FROM [UML_CMS].[dbo].[tblgasratehistory]"
            CommandText &= " WHERE fksiteid = " & siteid
            CommandText &= " and ratestdate = '" & CDate(start_date).ToString("dd MMM yyyy") & "'"
            CommandText &= " and ctrctacnum = '" & ImportDescription & "'"
            CO = New SqlCommand(CommandText, myConnection)
            contractid = CInt(CO.ExecuteScalar)
            
            ' tidy up the Usage's
            usage_day = usage_day.Replace(",", "")
            usage_night = usage_night.Replace(",", "")
            usage_allunit = usage_allunit.Replace(",", "")

            If usage_day = "N/A" Or usage_day = "" Then : usage_day = 0 : End If
            If usage_night = "N/A" Or usage_night = "" Then : usage_night = 0 : End If
            If usage_allunit = "N/A" Or usage_allunit = "" Then : usage_allunit = 0 : End If

            If usage_day.Length > 0 And Not IsNumeric(Left(usage_day, 1)) Then : usage_day = usage_day.Replace(Left(usage_day, 1), "") : End If
            If usage_night.Length > 0 And Not IsNumeric(Left(usage_night, 1)) Then : usage_night = usage_night.Replace(Left(usage_night, 1), "") : End If
            If usage_allunit.Length > 0 And Not IsNumeric(Left(usage_allunit, 1)) Then : usage_allunit = usage_allunit.Replace(Left(usage_allunit, 1), "") : End If

            usage_day = CInt(usage_day)
            usage_night = CInt(usage_night)
            usage_allunit = CInt(usage_allunit)

            
            If rate_allunit.Length = 0 Then rate_allunit = "0"
            If rate_day.Length = 0 Then rate_day = "0"
            If rate_night.Length = 0 Then rate_night = "0"

            If standingcharge_month.Length = 0 Then standingcharge_month = "0"
            If commission_kwh.Length = 0 Then commission_kwh = "0"
            If commission_year.Length = 0 Then commission_year = "0"

            'contract doesnt already exist
            If contractid < 1 Then
                CO = New SqlCommand
                CommandText = "INSERT INTO [UML_CMS].[dbo].[tblgastenders]"
                CommandText &= "([siteid],[tenderdesc], [tenderstatus], [wonlost], [tenderclsddate], [FKtenderproduct])"

                CommandText &= " VALUES "
                CommandText &= " (" & siteid & ", '" & ImportDescription & "', 1, 1, '" & CDate(strnow).ToString("dd MMM yyyy") & "', " & tenderproductid & ");"
                CommandText &= "SELECT @@IDENTITY;"
                CO = New SqlCommand(CommandText, myConnection)
                tenderid = CInt(CO.ExecuteScalar)

                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "INSERT INTO [UML_CMS].[dbo].[tblgasrates]"
                CommandText &= "([gastenderid],[supplierid],[gastendrate],[ctrctlength],[gasmeterchgpa],[suppliercommision],[supplierfixedcomm]) "
                CommandText &= " VALUES "
                CommandText &= " (" & tenderid & ", " & supplierid & ", '" & rate_allunit & "', DATEDIFF(MONTH, '" & CDate(start_date).ToString("dd MMM yyyy") & "','" & CDate(end_date).ToString("dd MMM yyyy") & "')+1, '" & standingcharge_month & "', '" & commission_kwh & "','" & CInt(commission_year) & "');"
                CommandText &= "SELECT @@IDENTITY;"
                CO = New SqlCommand(CommandText, myConnection)
                rateid = CInt(CO.ExecuteScalar)

                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "INSERT INTO [UML_CMS].[dbo].[tblgasratehistory]"
                CommandText &= "([fksiteid],[rateid],[tenderid],[ratestdate],[rateenddate],[ctrctacnum],[regconfirmed]) "
                CommandText &= "VALUES("
                CommandText &= siteid & ", " & rateid & ", " & tenderid & ", '" & CDate(start_date).ToString("dd MMM yyyy") & "', '" & CDate(end_date).ToString("dd MMM yyyy") & "',''"
                If registration_confirmed = "Yes" Then
                    CommandText &= ",1);"
                Else
                    CommandText &= ",0);"
                End If
                CommandText &= "SELECT @@IDENTITY;"
                CO = New SqlCommand(CommandText, myConnection)
                contractid = CInt(CO.ExecuteScalar)


                'supply detail already exists for site
                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "SELECT gassupplyid FROM [UML_CMS].[dbo].[tblgassupplydetails] WHERE [siteid] = " & siteid
                CO = New SqlCommand(CommandText, myConnection)
                supplydetailid = CInt(CO.ExecuteScalar)

                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "SELECT StatusID FROM [UML_CMS].[dbo].[tbltenderstatus] WHERE [StatusMain] = '" & tender_status & "'"
                CO = New SqlCommand(CommandText, myConnection)
                Dim tenderstatusid As Integer = -1
                tenderstatusid = CInt(CO.ExecuteScalar)


                If supplydetailid = 0 Then
                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "INSERT INTO [UML_CMS].[dbo].[tblgassupplydetails]"
                    CommandText &= "([siteid],[nominatedkwh],[ctrctstatus], [DMorNDM]) "
                    CommandText &= "VALUES("
                    CommandText &= siteid & ", '" & usage_allunit & "', " & tenderstatusid & ", '" & read_frequency & "');"
                    CommandText &= "SELECT @@IDENTITY;"
                    CO = New SqlCommand(CommandText, myConnection)
                    supplydetailid = CInt(CO.ExecuteScalar)

                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "INSERT INTO [UML_CMS].[dbo].[tblgassupplyaqs]"
                    CommandText &= "([creator_fk], [editor_fk], [meter_fk], [annualquantity]) "
                    CommandText &= "VALUES(349,349, " & meterid & ", '" & usage_allunit & "');"
                    CommandText &= "SELECT @@IDENTITY;"
                    CO = New SqlCommand(CommandText, myConnection)
                    CO.ExecuteNonQuery()


                Else
                    Dim kwh As Double = -1
                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "SELECT TOP 1 annualquantity FROM [UML_CMS].[dbo].[tblgassupplyaqs]"
                    CommandText &= " WHERE [meter_fk] = " & meterid & " ORDER BY [modified] DESC"
                    CO = New SqlCommand(CommandText, myConnection)
                    kwh = CDbl(CO.ExecuteScalar)
                    If usage_allunit <> kwh And usage_allunit > 0 Then
                        CO = New SqlCommand
                        CO.CommandType = CommandType.Text
                        CommandText = "  UPDATE [UML_CMS].[dbo].[tblgassupplydetails]"
                        CommandText &= " SET [nominatedkwh] = '" & usage_allunit & "'"
                        CommandText &= ", [DMorNDM] = '" & read_frequency & "'"
                        CommandText &= " WHERE [gassupplyid] = " & supplydetailid
                        CO = New SqlCommand(CommandText, myConnection)
                        CO.ExecuteNonQuery()

                        CO = New SqlCommand
                        CO.CommandType = CommandType.Text
                        CommandText = "INSERT INTO [UML_CMS].[dbo].[tblgassupplyaqs]"
                        CommandText &= "([creator_fk], [editor_fk], [meter_fk], [annualquantity]) "
                        CommandText &= "VALUES(349,349, " & meterid & ", '" & usage_allunit & "');"
                        CommandText &= "SELECT @@IDENTITY;"
                        CO = New SqlCommand(CommandText, myConnection)
                        CO.ExecuteNonQuery()
                    End If
                End If


            End If

        End If
        Dim complete As Boolean = True
        myConnection.Close()

    End Sub

    Protected Sub btnRunImport_Click(sender As Object, e As EventArgs) Handles btnRunImport.Click
        LoadX()
    End Sub
End Class
