Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class _elec_contract
    Inherits System.Web.UI.Page

    Dim strFile As String = "data\EAS-4747.csv"
    Dim strConnection As String = "Data Source=uk-ed0-sqlcl-01.MCEG.local;Initial Catalog=UML_CMS;Trusted_Connection=True;"
    Dim CommandText As String = ""
    Dim myConnection As SqlConnection
    Dim CO As SqlCommand

    Dim ImportDescription As String = Left("Bulk Import - " & strFile.ToString.Replace("data\", ""), 50)

    Dim strnow As String = Now.Date.ToString("dd MMM yyyy")

    Dim isCollectiveEnergy As Integer = 1

    Dim fuel As String = ""

    Dim site_id As String = ""

    Dim supplier_name As String = ""
    Dim start_date As String = ""
    Dim end_date As String = ""

    Dim rate_allunit As String = ""
    Dim rate_day As String = ""
    Dim rate_night As String = ""

    Dim usage_allunit As String = ""
    Dim usage_day As String = ""
    Dim usage_night As String = ""
    Dim usage_total As Integer = 0

    Dim standingcharge_month As String = ""
    Dim commission_kwh As String = ""
    Dim commission_year As String = ""

    Dim contract_type As String = ""
    Dim price_type As String = ""

    Dim tender_status As String = ""
    Dim termination_optout As String = ""
    Dim registration_confirmed As String = ""

    Dim contract_id As String = ""

    Dim tenderid As Integer = -1
    Dim rateid As Integer = -1
    Dim contractid As Integer = -1
    Dim supplydetailid As Integer = -1

    Dim contractlength As Integer = -1

    Dim rate_fit As String = ""


    Dim mCSV As New CSVData

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.lbloutput.Text = ImportDescription
        Try
            ResetVariables()

            If lblBeforeCustomers.Text = 0 Then
                CommandText = "SELECT count(*) FROM tblCustomer"
                myConnection = New SqlConnection(strConnection)
                CO = New SqlCommand(CommandText, myConnection)
                myConnection.Open()
                lblBeforeCustomers.Text = Convert.ToString(CO.ExecuteScalar())
            End If

        Catch aa As Exception
            System.Diagnostics.Debug.WriteLine(aa.Message.ToString)
        End Try


    End Sub

    Public Sub LoadX()
        '

        ' Set the separator and text qualifier

        mCSV.Separator = ","
        mCSV.TextQualifier = """"
        '
        ' Load in the CSV
        '
        mCSV.LoadCSV(Server.MapPath(strFile), True)
        '
        ' Clear the listview of any data and columns
        '
        Dim maxcol As Integer = mCSV.CSVDataSet.Tables(0).Columns.Count - 1
        Dim dt As DataTable = mCSV.CSVDataSet.Tables(0)
        ProcessBulkDataTable(dt)

    End Sub

    Private Sub ResetVariables()

        fuel = ""

        site_id = ""

        supplier_name = ""
        start_date = ""
        end_date = ""

        rate_allunit = ""
        rate_day = ""
        rate_night = ""

        usage_allunit = ""
        usage_day = ""
        usage_night = ""

        standingcharge_month = ""
        commission_kwh = ""
        commission_year = ""

        contract_type = ""
        price_type = ""

        tender_status = ""
        termination_optout = ""
        registration_confirmed = ""

        contract_id = ""

        tenderid = -1
        rateid = -1
        contractid = -1
        supplydetailid = -1

        contractlength = -1

        rate_fit = ""

    End Sub

    Private Function getfirst(ByVal str As String) As String
        Dim out As String = ""

        Dim chrPos As Integer = 0
        Dim chrMax As Integer = str.Length

        Do While chrPos < chrMax
            If chrPos > 0 Then
                If Char.IsUpper(str, chrPos) Then
                    If Char.IsLower(str, chrPos - 1) Then
                        Exit Do
                    End If
                    If str(chrPos - 1).ToString = ")" Then
                        Exit Do
                    End If
                    If Char.IsDigit(str, chrPos - 1) Then
                        If Not Char.IsWhiteSpace(str, chrPos + 1) Then
                            Exit Do
                        End If
                    End If
                End If
                If Char.IsDigit(str, chrPos) Then
                    If Char.IsLower(str, chrPos - 1) Then
                        Exit Do
                    End If
                End If
                If str(chrPos).ToString = "," Then
                    Exit Do
                End If
            End If
            out += str(chrPos).ToString
            chrPos += 1
        Loop
        Return out
    End Function

    Private Sub ProcessBulkDataTable(ByVal dt As DataTable)

        System.Diagnostics.Debug.WriteLine(ImportDescription)

        Dim dr As DataRow
        Dim iRowIndex As Integer = 0
        For Each dr In dt.Rows
            iRowIndex += 1
            ResetVariables()

            fuel = dr("fuel").ToString.Replace("'", "")

            site_id = dr("site_id").ToString.Replace("'", "")

            supplier_name = dr("supplier_name").ToString.Replace("'", "")
            start_date = dr("start_date").ToString.Replace("'", "")
            end_date = dr("end_date").ToString.Replace("'", "")

            If Not IsDate(start_date) Then
                MsgBox(start_date)
            End If

            If Not IsDate(end_date) Then
                MsgBox(end_date)
            End If

            contractlength = DateDiff(DateInterval.Month, CDate(start_date), CDate(end_date))

            rate_allunit = dr("rate_allunit").ToString.Replace("'", "")
            rate_day = dr("rate_day").ToString.Replace("'", "")
            rate_night = dr("rate_night").ToString.Replace("'", "")

            usage_allunit = dr("usage_allunit").ToString.Replace("'", "")
            usage_day = dr("usage_day").ToString.Replace("'", "")
            usage_night = dr("usage_night").ToString.Replace("'", "")

            standingcharge_month = dr("standingcharge_month").ToString.Replace("'", "")
            commission_kwh = dr("commission_kwh").ToString.Replace("'", "")
            commission_year = dr("commission_year").ToString.Replace("'", "")

            contract_type = dr("contract_type").ToString.Replace("'", "")

            If contract_type.StartsWith("Fixed") Then
                price_type = "Fully Fixed"
            End If

            If contract_type.StartsWith("Flexible") Then
                price_type = "Flexible"
            End If


            tender_status = dr("tender_status").ToString.Replace("'", "")
            termination_optout = dr("termination_optout").ToString.Replace("'", "")
            registration_confirmed = dr("registration_confirmed").ToString.Replace("'", "")

            contract_id = dr("contract_id").ToString.Replace("'", "")


            If Len(contract_id) = 0 Then

                Dim part_contracttype As String() = contract_type.Split(" ")
                Dim part_supplier As String() = supplier_name.Split(" ")
                Dim part_month As String = CStr(MonthName(DatePart(DateInterval.Month, CDate(end_date)), True))
                Dim part_year As String = CStr(DatePart(DateInterval.Year, CDate(end_date)))
                Dim part_contracttypeoutput As String = ""
                If part_contracttype(0) = "Fixed" Then
                    part_contracttypeoutput = "Fixed"
                ElseIf part_contracttype(0) = "Flexible" Then
                    part_contracttypeoutput = "Flex"
                End If

                contract_id = CStr(part_contracttypeoutput & "_" & part_supplier(0) & "_" & part_month & part_year)
                contract_id = contract_id.ToLower

            End If

            Try
                rate_fit = dr("rate_fit").ToString.Replace("'", "")
            Catch ex As Exception
                rate_fit = -1
            End Try

            SetVariables()
            If tenderid = -1 Then
                'System.Diagnostics.Debug.WriteLine(iRowIndex & " | " & customer_name & " | " & site_name & " | tender: " & tenderid & " | history: " & contractid & " (" & iRowIndex & "/" & dt.Rows.Count & ")")
                SetVariables()
                'System.Diagnostics.Debug.WriteLine(iRowIndex & " | " & customer_name & " | " & site_name & " | tender: " & tenderid & " | history: " & contractid & " (" & iRowIndex & "/" & dt.Rows.Count & ")")
            End If

            System.Diagnostics.Debug.WriteLine(iRowIndex & " / " & dt.Rows.Count & " (" & (iRowIndex / dt.Rows.Count) * 100 & " %)")
            Me.lbloutput.Text &= "<br />" & iRowIndex & " | " & " | tender: " & tenderid & " | history: " & contractid & " | contract: " & contract_id & " (" & iRowIndex & "/" & dt.Rows.Count & ")"

        Next

        CommandText = "SELECT count(*) FROM tblCustomer"
        myConnection = New SqlConnection(strConnection)
        CO = New SqlCommand(CommandText, myConnection)
        myConnection.Open()
        lblAfterCustomers.Text = Convert.ToString(CO.ExecuteScalar())

    End Sub

    Private Sub SetVariables()


        myConnection = New SqlConnection(strConnection)
        myConnection.Open()
        CO = New SqlCommand

        Dim custid As Integer = -1
        Dim siteid As Integer = -1
        Dim supplierid As Integer = -1
        Dim contractypeid As Integer = -1


        siteid = site_id

        ' work out if the end date of the "latest" contract in CM is between the import - if so move start date of the import to end date +1 of CM
        Dim latestenddate As DateTime

        CO.CommandType = CommandType.Text
        CommandText = "SELECT TOP 1 rateenddate FROM dbo.tblelecratehistory WHERE siteid = " & siteid & " ORDER BY rateenddate DESC"
        CO = New SqlCommand(CommandText, myConnection)
        latestenddate = CDate(CO.ExecuteScalar)


        If latestenddate >= CDate(start_date) And latestenddate <= CDate(end_date) Then
            start_date = DateAdd(DateInterval.Day, 1, latestenddate)
        End If


        'supplier name
        CO.CommandType = CommandType.Text
        CommandText = "SELECT SupplierId FROM [UML_CMS].[dbo].[tblsuppliers] WHERE Supplier_name = '" & supplier_name & "'"
        CO = New SqlCommand(CommandText, myConnection)
        supplierid = CInt(CO.ExecuteScalar)
        If Not supplierid > 0 Then
            supplierid = -1
        End If

        'contract type
        CO.CommandType = CommandType.Text
        CommandText = "SELECT ID FROM dbo.tbl_Purchasing_BasketAdministration WHERE iType = 'E' AND BasketDescription = '" & contract_type & "'"
        CO = New SqlCommand(CommandText, myConnection)
        contractypeid = CInt(CO.ExecuteScalar)
        If Not contractypeid > 0 Then
            contractypeid = -1
        End If

        Dim tenderproductid As Integer = contractypeid


        Dim siteexists As Integer = -1
        CO = New SqlCommand
        CO.CommandType = CommandType.Text
        CommandText = "SELECT CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END FROM dbo.tblsites WHERE siteid = " & siteid
        CO = New SqlCommand(CommandText, myConnection)
        siteexists = CInt(CO.ExecuteScalar)

        If siteexists = 1 Then
            ' sets tender complete
            CO = New SqlCommand
            CO.CommandType = CommandType.Text
            CommandText = "UPDATE tblsites SET statusid = 4 WHERE siteid = " & siteid
            CO = New SqlCommand(CommandText, myConnection)
            CO.ExecuteNonQuery()

            CO = New SqlCommand
            CO.CommandType = CommandType.Text
            CommandText = "SELECT CASE WHEN LEN(ISNULL(Address_1,'')) = 0 THEN 1 ELSE 0 END from tblsites where siteid = " & siteid
            CO = New SqlCommand(CommandText, myConnection)
            Dim addressupdate As Integer = -1
            addressupdate = CInt(CO.ExecuteScalar)

            ' contract stuff
            Select Case supplierid
                Case Nothing
                    supplierid = 42
            End Select

            CommandText = "SELECT ratehistid FROM [UML_CMS].[dbo].[tblelecratehistory]"
            CommandText &= " WHERE siteid = " & siteid
            CommandText &= " and ratestdate = '" & CDate(start_date).ToString("dd MMM yyyy") & "'"
            CommandText &= " and ctrctacnum = '" & contract_id & "'"
            CO = New SqlCommand(CommandText, myConnection)
            contractid = CInt(CO.ExecuteScalar)

            ' tidy up the Usage's
            usage_day = usage_day.Replace(",", "")
            usage_night = usage_night.Replace(",", "")
            usage_allunit = usage_allunit.Replace(",", "")

            If usage_day = "N/A" Or usage_day = "" Then : usage_day = 0 : End If
            If usage_night = "N/A" Or usage_night = "" Then : usage_night = 0 : End If
            If usage_allunit = "N/A" Or usage_allunit = "" Then : usage_allunit = 0 : End If

            If usage_day.Length > 0 And Not IsNumeric(Left(usage_day, 1)) Then : usage_day = usage_day.Replace(Left(usage_day, 1), "") : End If
            If usage_night.Length > 0 And Not IsNumeric(Left(usage_night, 1)) Then : usage_night = usage_night.Replace(Left(usage_night, 1), "") : End If
            If usage_allunit.Length > 0 And Not IsNumeric(Left(usage_allunit, 1)) Then : usage_allunit = usage_allunit.Replace(Left(usage_allunit, 1), "") : End If

            usage_day = CInt(usage_day)
            usage_night = CInt(usage_night)
            usage_allunit = CInt(usage_allunit)
            usage_total = CInt(usage_day) + CInt(usage_night)

            If rate_allunit.Length = 0 Then rate_allunit = "0"
            If rate_day.Length = 0 Then rate_day = "0"
            If rate_night.Length = 0 Then rate_night = "0"

            If standingcharge_month.Length = 0 Then standingcharge_month = "0"
            If commission_kwh.Length = 0 Then commission_kwh = "0"
            If commission_year.Length = 0 Then commission_year = "0"

            'contract doesnt already exist
            If contractid < 1 Then
                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "INSERT INTO [UML_CMS].[dbo].[tbltenders]"
                CommandText &= "([siteid],[tenderdesc],[rensupplierid], [FKtenderproduct],[newsupplierstdate], [tenderstatus], [wonlost], [tenderclsddate]) "
                CommandText &= " VALUES "
                CommandText &= " (" & siteid & ", '" & ImportDescription & "', " & supplierid & ", " & tenderproductid & ", '" & CDate(start_date).ToString("dd MMM yyyy") & "', 1, 1, '" & CDate(strnow).ToString("dd MMM yyyy") & "');"
                CommandText &= "SELECT @@IDENTITY;"
                CO = New SqlCommand(CommandText, myConnection)
                tenderid = CInt(CO.ExecuteScalar)

                If standingcharge_month = "" Then : standingcharge_month = 0 : End If
                If Not IsNumeric(Left(standingcharge_month, 1)) Then
                    standingcharge_month = standingcharge_month.Replace(Left(standingcharge_month, 1), "")
                End If

                '/* ******* HACK START ******* */

                Dim dayband As Integer = -1
                Dim nightband As Integer = -1
                Dim allband As Integer = -1
                Dim fitband As Integer = -1

                If price_type = "Flexible" Then

                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "EXEC usp_bulkimport_electricity_supplierbands @supplierid=" & supplierid & ", @rate_type='Flexible Single Rate'"
                    CO = New SqlCommand(CommandText, myConnection)
                    allband = CInt(CO.ExecuteScalar)

                    dayband = 0
                    nightband = 0

                Else
                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "EXEC usp_bulkimport_electricity_supplierbands @supplierid=" & supplierid & ", @rate_type='Day'"
                    CO = New SqlCommand(CommandText, myConnection)
                    dayband = CInt(CO.ExecuteScalar)

                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "EXEC usp_bulkimport_electricity_supplierbands @supplierid=" & supplierid & ", @rate_type='Night'"
                    CO = New SqlCommand(CommandText, myConnection)
                    nightband = CInt(CO.ExecuteScalar)

                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "EXEC usp_bulkimport_electricity_supplierbands @supplierid=" & supplierid & ", @rate_type='All'"
                    CO = New SqlCommand(CommandText, myConnection)
                    allband = CInt(CO.ExecuteScalar)
                    '''''' Collective HACK ''''''
                    If isCollectiveEnergy = 1 Then
                        If usage_day > 0 Then
                            dayband = 18
                            nightband = 19
                            fitband = 998
                        Else
                            allband = 95
                            fitband = 999
                        End If
                    End If
                    '''''' HACK ''''''
                End If

                If dayband = 0 And nightband = 0 And allband = 0 Then
                    MsgBox("Missing bands for supplier: " & supplierid)
                    Me.lbloutput.Text &= "<br />" & "Missing bands for supplier: " & supplierid
                    System.Diagnostics.Debug.WriteLine("Missing bands for supplier: " & supplierid)
                End If


                '' this point need to work out the STOD ID

                '' STOD depends on the supplier

                Dim stodid As Integer = -1
                If price_type = "Flexible" Then
                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "EXEC usp_bulkimport_electricity_stodbands @supplierid=" & supplierid & ", @rate_type='Flex'"
                    CO = New SqlCommand(CommandText, myConnection)
                    stodid = CInt(CO.ExecuteScalar)
                ElseIf usage_day > 0 Then
                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "EXEC usp_bulkimport_electricity_stodbands @supplierid=" & supplierid & ", @rate_type='Dual'"
                    CO = New SqlCommand(CommandText, myConnection)
                    stodid = CInt(CO.ExecuteScalar)
                Else
                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "EXEC usp_bulkimport_electricity_stodbands @supplierid=" & supplierid & ", @rate_type='All'"
                    CO = New SqlCommand(CommandText, myConnection)
                    stodid = CInt(CO.ExecuteScalar)
                End If
                '''''' Collective HACK ''''''
                If isCollectiveEnergy = 1 Then
                    If usage_day > 0 Then
                        stodid = 16
                    Else
                        stodid = 53
                    End If
                End If
                '''''' HACK ''''''


                '/* ******* HACK END ******* */

                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "INSERT INTO [UML_CMS].[dbo].[tblrates]"
                CommandText &= "([tenderid],[stodid],[supplierid],[stgchg],[ctrctlength],[pricetype],[supplierfixedcomm],[suppliercommision],[pricepulled]) "
                CommandText &= " VALUES "
                CommandText &= " (" & tenderid & ", " & stodid & ", " & supplierid & ", '" & standingcharge_month & "', " & contractlength & ", '" & price_type & "','" & commission_year & "', '" & commission_kwh & "',0);"
                CommandText &= "SELECT @@IDENTITY;"
                CO = New SqlCommand(CommandText, myConnection)
                rateid = CInt(CO.ExecuteScalar)

                If Not dayband = -1 Or Not allband = -1 Then

                    If usage_day > 0 Then

                        CO = New SqlCommand
                        CO.CommandType = CommandType.Text
                        CommandText = "INSERT INTO [UML_CMS].[dbo].[tblbandrate]"
                        CommandText &= "([rateid],[bandid],[rate],[consumption],[lafconsumption]) "
                        CommandText &= " VALUES "
                        CommandText &= " (" & rateid & ", " & dayband & ", '" & rate_day & "', '" & usage_day & "', 0);"
                        CommandText &= "SELECT @@IDENTITY;"
                        CO = New SqlCommand(CommandText, myConnection)
                        CO.ExecuteNonQuery()

                        CO = New SqlCommand
                        CO.CommandType = CommandType.Text
                        CommandText = "INSERT INTO [UML_CMS].[dbo].[tblbandrate]"
                        CommandText &= "([rateid],[bandid],[rate],[consumption],[lafconsumption]) "
                        CommandText &= " VALUES "
                        CommandText &= " (" & rateid & ", " & nightband & ", '" & rate_night & "', '" & usage_night & "', 0);"
                        CommandText &= "SELECT @@IDENTITY;"
                        CO = New SqlCommand(CommandText, myConnection)
                        CO.ExecuteNonQuery()

                        '''''' Collective HACK ''''''
                        If isCollectiveEnergy = 1 Then
                            CO = New SqlCommand
                            CO.CommandType = CommandType.Text
                            CommandText = "INSERT INTO [UML_CMS].[dbo].[tblbandrate]"
                            CommandText &= "([rateid],[bandid],[rate],[consumption],[lafconsumption]) "
                            CommandText &= " VALUES "
                            CommandText &= " (" & rateid & ", " & fitband & ", '" & rate_fit & "', '0', 0);"
                            CommandText &= "SELECT @@IDENTITY;"
                            CO = New SqlCommand(CommandText, myConnection)
                            CO.ExecuteNonQuery()
                        End If
                    Else
                        CO = New SqlCommand
                        CO.CommandType = CommandType.Text
                        CommandText = "INSERT INTO [UML_CMS].[dbo].[tblbandrate]"
                        CommandText &= "([rateid],[bandid],[rate],[consumption],[lafconsumption]) "
                        CommandText &= " VALUES "
                        CommandText &= " (" & rateid & ", " & allband & ", '" & rate_allunit & "', '" & CInt(usage_allunit) & "', 0);"
                        CommandText &= "SELECT @@IDENTITY;"
                        CO = New SqlCommand(CommandText, myConnection)
                        CO.ExecuteNonQuery()
                        
                        '''''' Collective HACK ''''''
                        If isCollectiveEnergy = 1 Then
                            CO = New SqlCommand
                            CO.CommandType = CommandType.Text
                            CommandText = "INSERT INTO [UML_CMS].[dbo].[tblbandrate]"
                            CommandText &= "([rateid],[bandid],[rate],[consumption],[lafconsumption]) "
                            CommandText &= " VALUES "
                            CommandText &= " (" & rateid & ", " & fitband & ", '" & rate_fit & "', '0', 0);"
                            CommandText &= "SELECT @@IDENTITY;"
                            CO = New SqlCommand(CommandText, myConnection)
                            CO.ExecuteNonQuery()
                        End If
                    End If
                End If


                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "INSERT INTO [UML_CMS].[dbo].[tblelecratehistory]"
                CommandText &= "([siteid],[rateid],[tenderid],[ratestdate],[rateenddate],[ctrctacnum],[dummyctrct],[regconfirmed]) "
                CommandText &= "VALUES("
                CommandText &= siteid & ", " & rateid & ", " & tenderid & ", '" & CDate(start_date).ToString("dd MMM yyyy") & "', '" & CDate(end_date).ToString("dd MMM yyyy") & "',  '" & contract_id & "', 0"
                If registration_confirmed.ToLower = "yes" Then
                    CommandText &= ",1);"
                Else
                    CommandText &= ",0);"
                End If
                CommandText &= "SELECT @@IDENTITY;"
                CO = New SqlCommand(CommandText, myConnection)
                contractid = CInt(CO.ExecuteScalar)

            End If

        End If
        Dim complete As Boolean = True
        myConnection.Close()

    End Sub

    Protected Sub btnRunImport_Click(sender As Object, e As EventArgs) Handles btnRunImport.Click
        LoadX()
    End Sub
End Class
