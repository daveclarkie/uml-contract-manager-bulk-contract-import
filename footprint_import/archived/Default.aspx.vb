Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class _Default
    Inherits System.Web.UI.Page

    Dim strFile As String = "data\Sunrise HH 2014.csv"
    Dim strConnection As String = "Data Source=uml-sql\umlsql2005;Initial Catalog=UML_CMS;User Id=sa;Password=chicago;"
    Dim CommandText As String = ""
    Dim myConnection As SqlConnection
    Dim CO As SqlCommand

    Dim ImportDescription As String = "Bulk Import - " & Now.Date.ToString("dd MMM yyyy")
    Dim strnow As String = Now.Date.ToString("dd MMM yyyy")
    Dim CustomerName As String = ""
    Dim FuelType As String = ""
    Dim RenewalDate As String = ""
    Dim MeterNumber As String = ""
    Dim SiteName As String = ""
    Dim CustomerSiteReference As String = ""
    Dim UMLSiteReference As String = ""
    Dim FootprintClientReference As String = ""
    Dim MeterTypeElec As String = ""
    Dim FootprintSiteReference As String = ""
    Dim SupplierName As String = ""
    Dim SourceSystem As String = ""

    Dim CustomerClass As String = ""

    Dim voltageid As Integer = -1
    Dim Voltage As String = ""
    Dim ASC As String = ""
    Dim AllUnitRate As String = ""
    Dim DayRate As String = ""
    Dim NightRate As String = ""
    Dim StandingCharge As String = ""
    Dim AllUnitUsage As String = ""
    Dim DayUsage As String = ""
    Dim NightUsage As String = ""
    Dim Commission As String = ""
    Dim FixedCommission As String = ""
    Dim EndDate As String = ""
    Dim MeterProfile As String = ""
    Dim LLF As String = ""
    Dim MTS As String = ""

    Dim tendertype As String = ""
    Dim terminationoptout As String = ""
    Dim registrationconfirmed As String = ""

    Dim analystname As String = ""
    Dim analystid As Integer = -1
    Dim accountmanangername As String = ""
    Dim accountmanangerid As Integer = -1

    Dim TenderStatus As String = ""

    Dim address1 As String = ""
    Dim address2 As String = ""
    Dim address3 As String = ""
    Dim address4 As String = ""
    Dim address5 As String = ""
    Dim postcode As String = ""

    Dim mCSV As New CSVData

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.lbloutput.Text = CDate(Now()).ToString("dd MMM yyyy")
        Try
            ResetVariables()

            If lblBeforeCustomers.Text = 0 Then
                CommandText = "SELECT count(*) FROM tblCustomer"
                myConnection = New SqlConnection(strConnection)
                CO = New SqlCommand(CommandText, myConnection)
                myConnection.Open()
                lblBeforeCustomers.Text = Convert.ToString(CO.ExecuteScalar())
            End If

        Catch aa As Exception
            System.Diagnostics.Debug.WriteLine(aa.Message.ToString)
        End Try


    End Sub

    Public Sub LoadX()
        '

        ' Set the separator and text qualifier

        mCSV.Separator = ","
        mCSV.TextQualifier = """"
        '
        ' Load in the CSV
        '
        mCSV.LoadCSV(Server.MapPath(strFile), True)
        '
        ' Clear the listview of any data and columns
        '
        Dim maxcol As Integer = mCSV.CSVDataSet.Tables(0).Columns.Count - 1
        Dim dt As DataTable = mCSV.CSVDataSet.Tables(0)
        ProcessBulkDataTable(dt)

    End Sub

    Private Sub ResetVariables()

        CustomerName = ""
        FuelType = ""
        RenewalDate = ""
        MeterNumber = ""
        SiteName = ""
        UMLSiteReference = ""
        FootprintClientReference = ""
        MeterTypeElec = ""
        FootprintSiteReference = ""
        SupplierName = ""
        SourceSystem = ""

        address1 = ""
        address2 = ""
        address3 = ""
        address4 = ""
        address5 = ""
        postcode = ""

        TenderStatus = ""

    End Sub

    Private Function getfirst(ByVal str As String) As String
        Dim out As String = ""

        Dim chrPos As Integer = 0
        Dim chrMax As Integer = str.Length

        Do While chrPos < chrMax
            If chrPos > 0 Then
                If Char.IsUpper(str, chrPos) Then
                    If Char.IsLower(str, chrPos - 1) Then
                        Exit Do
                    End If
                    If str(chrPos - 1).ToString = ")" Then
                        Exit Do
                    End If
                    If Char.IsDigit(str, chrPos - 1) Then
                        If Not Char.IsWhiteSpace(str, chrPos + 1) Then
                            Exit Do
                        End If
                    End If
                End If
                If Char.IsDigit(str, chrPos) Then
                    If Char.IsLower(str, chrPos - 1) Then
                        Exit Do
                    End If
                End If
                If str(chrPos).ToString = "," Then
                    Exit Do
                End If
            End If
            out += str(chrPos).ToString
            chrPos += 1
        Loop
        Return out
    End Function

    Private Sub ProcessBulkDataTable(ByVal dt As DataTable)

        Dim dr As DataRow
        Dim iRowIndex As Integer = 0
        For Each dr In dt.Rows
            iRowIndex += 1
            ResetVariables()

            CustomerName = dr("Customer_Name").ToString.Replace("'", "")
            FuelType = dr("Fuel_Type").ToString.Replace("'", "")
            RenewalDate = dr("Start Date").ToString
            MeterNumber = dr("Meter_Number").ToString.Replace("'", "")

            SiteName = dr("Site_Name").ToString.Replace("'", "")
            CustomerSiteReference = "-1"

            UMLSiteReference = dr("UML_Site_Reference").ToString.Replace("'", "")
            FootprintClientReference = dr("Footprint_Client_Reference").ToString.Replace("'", "")
            MeterTypeElec = dr("Meter_Type_Elec").ToString.Replace("'", "")
            FootprintSiteReference = dr("Footprint_Site_Reference").ToString.Replace("'", "")
            SupplierName = dr("Supplier_Name").ToString.Replace("'", "")

            address1 = dr("Address").ToString.Replace("'", "")
            address2 = dr("Address 2").ToString.Replace("'", "")
            address3 = dr("Address 3").ToString.Replace("'", "")
            address4 = ""
            address5 = ""
            postcode = dr("Postcode").ToString.Replace("'", "")

            Voltage = dr("Voltage").ToString.Replace("'", "")
            ASC = dr("ASC").ToString.Replace("'", "")
            AllUnitRate = dr("All Unit Rate").ToString.Replace("'", "")
            DayRate = dr("Day Rate").ToString.Replace("'", "")
            NightRate = dr("Night Rate").ToString.Replace("'", "")
            StandingCharge = dr("Standing Charge Month").ToString.Replace("'", "")
            AllUnitUsage = dr("All Unit Usage").ToString.Replace("'", "")
            DayUsage = dr("Day Usage").ToString.Replace("'", "")
            NightUsage = dr("Night Usage").ToString.Replace("'", "")
            Commission = dr("Commission P kwh").ToString.Replace("'", "")
            FixedCommission = dr("Fixed Commission Per year").ToString.Replace("'", "")
            EndDate = dr("End Date").ToString.Replace("'", "")
            MeterProfile = dr("Profile").ToString.Replace("'", "")
            LLF = dr("LLF").ToString.Replace("'", "")
            MTS = dr("MTS").ToString.Replace("'", "")

            tendertype = dr("Contract Type").ToString.Replace("'", "")
            terminationoptout = dr("Termination Opt Out").ToString.Replace("'", "")
            registrationconfirmed = dr("Registration Confirmed").ToString.Replace("'", "")

            CustomerClass = dr("Customer Class").ToString.Replace("'", "")

            analystname = dr("Analyst").ToString.Replace("'", "")
            accountmanangername = dr("AM_Name").ToString.Replace("'", "")

            TenderStatus = dr("Tender Status").ToString.Replace("'", "")

            SetVariables()

            Me.lbloutput.Text &= "<br />" & iRowIndex & " | " & CustomerName & " | " & SiteName & " (" & iRowIndex & "/" & dt.Rows.Count & ")"

        Next

        CommandText = "SELECT count(*) FROM tblCustomer"
        myConnection = New SqlConnection(strConnection)
        CO = New SqlCommand(CommandText, myConnection)
        myConnection.Open()
        lblAfterCustomers.Text = Convert.ToString(CO.ExecuteScalar())

    End Sub

    Private Sub SetVariables()


        myConnection = New SqlConnection(strConnection)
        myConnection.Open()
        CO = New SqlCommand

        If SiteName = "N/A" Then
            SiteName = address1 & " - " & Right(MeterNumber, 3)
        End If
        If SiteName.Contains("'") Then
            SiteName = SiteName.Replace("'", "")
        End If
        Dim custid As Integer = -1
        Dim siteid As Integer = -1




        'voltage
        CO = New SqlCommand
        CO.CommandType = CommandType.Text
        CommandText = "SELECT * FROM dbo.tbllkpvolt WHERE voltagedesc = '" & Voltage & "'"
        CO = New SqlCommand(CommandText, myConnection)
        voltageid = CInt(CO.ExecuteScalar)
        If Not voltageid > 0 Then
            voltageid = -1
        End If

        'analyst
        CO.CommandType = CommandType.Text
        CommandText = "SELECT AccmgrId FROM dbo.tblaccmgr WHERE Accountmgr = '" & analystname & "'"
        CO = New SqlCommand(CommandText, myConnection)
        analystid = CInt(CO.ExecuteScalar)
        If Not analystid > 0 Then
            analystid = -1
        End If

        'account mananger
        CO.CommandType = CommandType.Text
        CommandText = "SELECT AccmgrId FROM dbo.tblaccmgr WHERE Accountmgr = '" & accountmanangername & "'"
        CO = New SqlCommand(CommandText, myConnection)
        accountmanangerid = CInt(CO.ExecuteScalar)
        If Not accountmanangerid > 0 Then
            accountmanangerid = -1
        End If


        If UMLSiteReference = "" Or UMLSiteReference = "N/A" Then
        Else
            Try
                siteid = UMLSiteReference
                CO.CommandType = CommandType.Text
                CommandText = "SELECT custid FROM tblSites WHERE siteid = " & siteid
                CO = New SqlCommand(CommandText, myConnection)
                custid = CInt(CO.ExecuteScalar)
            Catch ex As Exception

            End Try
        End If

        ' for each row in dt

        '   check to see if customer matches
        '   create new customer / retrieve customer details

        If custid = -1 Then

            CommandText = "SELECT custid FROM tblCustomer WHERE customername = '" & CustomerName & "'"
            CO = New SqlCommand(CommandText, myConnection)
            custid = CInt(CO.ExecuteScalar())
            If custid = 0 Then
                CommandText = "INSERT INTO tblCustomer (customername, address_1, address_2, address_3, address_4, pcode, custorprospect, intMandCRef) VALUES ('" & Trim(CustomerName) & "','" & Trim(address1) & "','" & Trim(address2) & "','" & Trim(address3) & "','" & Trim(address4) & "','" & Trim(postcode) & "', 2, '" & FootprintClientReference & "');"
                CommandText &= "SELECT @@IDENTITY;"
                CO = New SqlCommand(CommandText, myConnection)
                custid = CInt(CO.ExecuteScalar)
            End If

        End If

        If accountmanangerid > 0 Then
            CO = New SqlCommand
            CO.CommandType = CommandType.Text
            CommandText = "UPDATE tblcustomer SET FKaccmgrid = " & accountmanangerid & " WHERE Custid = " & custid
            CO = New SqlCommand(CommandText, myConnection)
            CO.ExecuteNonQuery()
        End If

        ' find if customer has class
        CommandText = "SELECT class from tblCustomer_class WHERE fkcustid = " & custid
        CO = New SqlCommand(CommandText, myConnection)
        Dim customerclass As String = ""
        customerclass = CStr(CO.ExecuteScalar)
        If Not customerclass = Nothing Then
            ' if it does and diff to new value then update
            If Not CStr(CO.ExecuteScalar) = customerclass Then
                CommandText = "UPDATE tblCustomer_class SET Class = '" & customerclass & "' WHERE fkcustid = " & custid
                CO = New SqlCommand(CommandText, myConnection)
                CO.ExecuteNonQuery()
            End If
        Else
            ' if it does not then insert
            CommandText = "INSERT INTO tblCustomer_class ([Class],[fkcustid]) VALUES ('" & customerclass & "'," & custid & ")"
            CO = New SqlCommand(CommandText, myConnection)
            CO.ExecuteNonQuery()
        End If


        If siteid = -1 Then
            '   check to see site matches
            CO.CommandType = CommandType.Text
            CommandText = "SELECT siteid FROM tblSites WHERE custid = " & custid & " and sitename like '" & SiteName & "'"
            CO = New SqlCommand(CommandText, myConnection)
            siteid = CInt(CO.ExecuteScalar)

            If siteid < 1 Then
                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "INSERT INTO [UML_CMS].[dbo].[tblsites] "
                CommandText &= " ([custid],[sitename],[deadoralive],[Address_1],[Address_2],[Address_3],[Address_4],[Pcode],[prodelecneg],[prodgasneg],[Customersiteref],[statusid])"
                CommandText &= " VALUES "
                CommandText &= "(" & custid & ", '" & Trim(SiteName) & "', 0,'" & Trim(Left(address1, 49)) & "','" & Trim(address2) & "','" & Trim(address3) & "','" & Trim(address4) & "','" & Trim(postcode) & "'"
                If FuelType.ToLower = "gas" Then
                    CommandText &= ", 0, -1"
                Else
                    CommandText &= ", -1, 0"
                End If
                CommandText &= ", '" & Trim(FootprintSiteReference) & "',4);"
                CommandText &= "select siteid from tblsites where sitename = '" & Trim(SiteName) & "' and pcode = '" & Trim(postcode) & "';"
                CO = New SqlCommand(CommandText, myConnection)
                siteid = CInt(CO.ExecuteScalar)
            End If

            

        End If



        Dim siteexists As Integer = -1
        CO = New SqlCommand
        CO.CommandType = CommandType.Text
        CommandText = "SELECT CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END FROM dbo.tblsites WHERE siteid = " & siteid
        CO = New SqlCommand(CommandText, myConnection)
        siteexists = CInt(CO.ExecuteScalar)

        If siteexists = 0 Then
            System.Diagnostics.Debug.WriteLine(siteexists)

        Else


            CO = New SqlCommand
            CO.CommandType = CommandType.Text
            CommandText = "UPDATE tblsites SET statusid = 4 WHERE siteid = " & siteid
            CO = New SqlCommand(CommandText, myConnection)
            CO.ExecuteNonQuery()

            If analystid > 0 Then
                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "UPDATE tblsites SET Accmgrid = " & analystid & " WHERE siteid = " & siteid
                CO = New SqlCommand(CommandText, myConnection)
                CO.ExecuteNonQuery()
            End If
            If voltageid > 0 Then
                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "UPDATE tblsites SET voltageID = " & voltageid & " WHERE siteid = " & siteid
                CO = New SqlCommand(CommandText, myConnection)
                CO.ExecuteNonQuery()
            End If
            If ASC.Length > 0 Then
                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "UPDATE tblsites SET kVA = '" & ASC & "' WHERE siteid = " & siteid
                CO = New SqlCommand(CommandText, myConnection)
                CO.ExecuteNonQuery()
            End If

            CO = New SqlCommand
            CO.CommandType = CommandType.Text
            CommandText = "SELECT CASE WHEN LEN(ISNULL(Address_1,'')) = 0 THEN 1 ELSE 0 END from tblsites where siteid = " & siteid
            CO = New SqlCommand(CommandText, myConnection)
            Dim addressupdate As Integer = -1
            addressupdate = CInt(CO.ExecuteScalar)

            If addressupdate = 1 Then
                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "UPDATE tblSites SET address_1 = '" & address1 & "', address_2 = '" & address2 & "', address_3 = '" & address3 & "', pcode = '" & postcode & "'  where siteid = " & siteid
                CO = New SqlCommand(CommandText, myConnection)
                CO.ExecuteNonQuery()
            End If

            If Not CustomerSiteReference = "" And Not CustomerSiteReference = "-1" Then
                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "UPDATE tblSites SET Customersiteref = '" & CustomerSiteReference & "', voltageid = 2 where siteid = " & siteid
                CO = New SqlCommand(CommandText, myConnection)
                CO.ExecuteNonQuery()
            End If

            '   check to see if meter is already attached to site?
            CO = New SqlCommand
            CO.CommandType = CommandType.Text
            If FuelType.ToLower = "gas" Then
                CommandText = "SELECT mnumid FROM tblmnumbers where mnumber like '" & MeterNumber & "'"
            Else
                CommandText = "SELECT pkmpanid FROM tblsitempans where distribution + supplier like '" & MeterNumber & "'"
            End If
            Dim meterid As Integer = -1
            CO = New SqlCommand(CommandText, myConnection)
            meterid = CO.ExecuteScalar

            If meterid < 1 And MeterNumber.Length > 0 Then
                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                If FuelType.ToLower = "gas" Then
                    CommandText = "INSERT INTO [UML_CMS].[dbo].[tblmnumbers]"
                    CommandText &= "([siteID],[Mnumber],[CRC_Required])"
                    CommandText &= " VALUES "
                    CommandText &= "(" & siteid & ",'" & MeterNumber & "',0);"
                    CommandText &= "SELECT @@IDENTITY;"
                    CO = New SqlCommand(CommandText, myConnection)
                    meterid = CInt(CO.ExecuteScalar)
                Else


                    If MeterProfile = "" Then MeterProfile = "00"
                    If MeterProfile.Length = 1 Then MeterProfile = "0" & MeterProfile
                    If MTS = "" Then : MTS = 0 : End If
                    If LLF = "" Then : LLF = 0 : End If

                    CommandText = "INSERT INTO [UML_CMS].[dbo].[tblsitempans]"
                    CommandText &= "([FKsite_id],[profiletype],[metertimeswitch],[LLF],[distribution],[supplier],[intByPassZeroCheck],[CRC_Required])"
                    CommandText &= " VALUES "
                    CommandText &= "(" & siteid & ", '" & Trim(MeterProfile) & "', '" & Trim(MTS) & "', '" & Trim(LLF) & "', '" & Trim(Left(MeterNumber, 2)) & "', '" & Trim(Right(MeterNumber, Len(MeterNumber) - 2)) & "', 1, 0);"
                    CommandText &= "SELECT @@IDENTITY;"
                    CO = New SqlCommand(CommandText, myConnection)
                    meterid = CInt(CO.ExecuteScalar)
                End If
            End If

            Dim toplineupdating As String = ""
            CO = New SqlCommand
            CO.CommandType = CommandType.Text
            CommandText = "select CASE WHEN metertimeswitch IS NULL THEN '000' ELSE metertimeswitch END [metertimeswitch] from tblsitempans where pkmpanid = " & meterid
            CO = New SqlCommand(CommandText, myConnection)
            toplineupdating = CO.ExecuteScalar

            If toplineupdating = "000" And MTS.Length > 0 Then
                If MeterProfile.Length = 1 Then MeterProfile = "0" & MeterProfile
                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "UPDATE tblsitempans SET profiletype = '" & Trim(MeterProfile) & "', metertimeswitch = '" & Trim(MTS) & "', LLF = '" & Trim(LLF) & "'  where pkmpanid = " & meterid
                CO = New SqlCommand(CommandText, myConnection)
                CO.ExecuteNonQuery()
            End If

            '   if gas / elec
            '       elec: elec contract
            '       gas:  gas contract

            Dim supplierid As Integer = -1
            Dim tenderid As Integer = -1
            Dim rateid As Integer = -1
            Dim contractid As Integer = -1
            Dim supplydetailid As Integer = -1

            CommandText = "SELECT SupplierId FROM [UML_CMS].[dbo].[tblsuppliers]"
            CommandText &= " WHERE Supplier_name = '" & SupplierName & "'"
            CO = New SqlCommand(CommandText, myConnection)
            supplierid = CInt(CO.ExecuteScalar)

            Select Case supplierid
                Case Nothing
                    supplierid = 42
            End Select


            If FuelType.ToLower = "gas" Then
                CommandText = "SELECT PKgasratehistid FROM [UML_CMS].[dbo].[tblgasratehistory]"
                CommandText &= " WHERE fksiteid = " & siteid
                CommandText &= " and ratestdate = '" & CDate(RenewalDate).ToString("dd MMM yyyy") & "'"
                CommandText &= " and ctrctacnum = '" & ImportDescription & "'"
                CO = New SqlCommand(CommandText, myConnection)
                contractid = CInt(CO.ExecuteScalar)
            Else
                CommandText = "SELECT ratehistid FROM [UML_CMS].[dbo].[tblelecratehistory]"
                CommandText &= " WHERE siteid = " & siteid
                CommandText &= " and ratestdate = '" & CDate(RenewalDate).ToString("dd MMM yyyy") & "'"
                CommandText &= " and ctrctacnum = '" & ImportDescription & "'"
                CO = New SqlCommand(CommandText, myConnection)
                contractid = CInt(CO.ExecuteScalar)
            End If

            'Remove "," from usage
            DayUsage = DayUsage.Replace(",", "")
            NightUsage = NightUsage.Replace(",", "")
            AllUnitUsage = AllUnitUsage.Replace(",", "")

            If DayUsage = "N/A" Or DayUsage = "" Then : DayUsage = 0 : End If
            If NightUsage = "N/A" Or NightUsage = "" Then : NightUsage = 0 : End If
            If AllUnitUsage = "N/A" Or AllUnitUsage = "" Then : AllUnitUsage = 0 : End If

            If DayUsage.Length > 0 And Not IsNumeric(Left(DayUsage, 1)) Then : DayUsage = DayUsage.Replace(Left(DayUsage, 1), "") : End If
            If NightUsage.Length > 0 And Not IsNumeric(Left(NightUsage, 1)) Then : NightUsage = NightUsage.Replace(Left(NightUsage, 1), "") : End If
            If AllUnitUsage.Length > 0 And Not IsNumeric(Left(AllUnitUsage, 1)) Then : AllUnitUsage = AllUnitUsage.Replace(Left(AllUnitUsage, 1), "") : End If

            DayUsage = CInt(DayUsage)
            NightUsage = CInt(NightUsage)
            AllUnitUsage = CInt(AllUnitUsage)


            If AllUnitRate.Length = 0 Then AllUnitRate = "0"
            If StandingCharge.Length = 0 Then StandingCharge = "0"
            If Commission.Length = 0 Then Commission = "0"
            If FixedCommission.Length = 0 Then FixedCommission = "0"


            Dim tenderproductid As Integer = -1
            If contractid < 1 Then
                If FuelType.ToLower = "gas" Then

                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "SELECT ID "
                    CommandText &= " FROM dbo.tbl_Purchasing_BasketAdministration  "
                    CommandText &= " WHERE iType = 'G'"
                    CommandText &= " AND BasketDescription = '" & tendertype & "'"
                    CO = New SqlCommand(CommandText, myConnection)
                    tenderproductid = CInt(CO.ExecuteScalar)


                    CO = New SqlCommand
                    CommandText = "INSERT INTO [UML_CMS].[dbo].[tblgastenders]"
                    CommandText &= "([siteid],[tenderdesc], [tenderstatus], [wonlost], [tenderclsddate], [FKtenderproduct])"

                    CommandText &= " VALUES "
                    CommandText &= " (" & siteid & ", '" & ImportDescription & "', 1, 1, '" & CDate(strnow).ToString("dd MMM yyyy") & "', " & tenderproductid & ");"
                    CommandText &= "SELECT @@IDENTITY;"
                    CO = New SqlCommand(CommandText, myConnection)
                    tenderid = CInt(CO.ExecuteScalar)

                    If AllUnitRate.Length = 0 Then AllUnitRate = "0"
                    If StandingCharge.Length = 0 Then StandingCharge = "0"
                    If Commission.Length = 0 Then Commission = "0"
                    If FixedCommission.Length = 0 Then FixedCommission = "0"

                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "INSERT INTO [UML_CMS].[dbo].[tblgasrates]"
                    CommandText &= "([gastenderid],[supplierid],[gastendrate],[ctrctlength],[gasmeterchgpa],[suppliercommision],[supplierfixedcomm]) "
                    CommandText &= " VALUES "
                    CommandText &= " (" & tenderid & ", " & supplierid & ", '" & AllUnitRate & "', DATEDIFF(MONTH, '" & CDate(RenewalDate).ToString("dd MMM yyyy") & "','" & CDate(EndDate).ToString("dd MMM yyyy") & "')+1, '" & StandingCharge & "', '" & Commission & "','" & CInt(FixedCommission) & "');"
                    CommandText &= "SELECT @@IDENTITY;"
                    CO = New SqlCommand(CommandText, myConnection)
                    rateid = CInt(CO.ExecuteScalar)

                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "INSERT INTO [UML_CMS].[dbo].[tblgasratehistory]"
                    CommandText &= "([fksiteid],[rateid],[tenderid],[ratestdate],[rateenddate],[ctrctacnum],[regconfirmed]) "
                    CommandText &= "VALUES("
                    CommandText &= siteid & ", " & rateid & ", " & tenderid & ", '" & CDate(RenewalDate).ToString("dd MMM yyyy") & "', '" & CDate(EndDate).ToString("dd MMM yyyy") & "','" & ImportDescription & "'"
                    If registrationconfirmed = "Yes" Then
                        CommandText &= ",1);"
                    Else
                        CommandText &= ",0);"
                    End If
                    CommandText &= "SELECT @@IDENTITY;"
                    CO = New SqlCommand(CommandText, myConnection)
                    contractid = CInt(CO.ExecuteScalar)


                    'supply detail already exists for site
                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "SELECT gassupplyid FROM [UML_CMS].[dbo].[tblgassupplydetails]"
                    CommandText &= " WHERE [siteid] = " & siteid
                    CO = New SqlCommand(CommandText, myConnection)
                    supplydetailid = CInt(CO.ExecuteScalar)

                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "SELECT StatusID FROM [UML_CMS].[dbo].[tbltenderstatus] "
                    CommandText &= " WHERE [StatusMain] = '" & TenderStatus & "'"
                    CO = New SqlCommand(CommandText, myConnection)
                    Dim tenderstatusid As Integer = -1
                    tenderstatusid = CInt(CO.ExecuteScalar)


                    If supplydetailid = 0 Then
                        CO = New SqlCommand
                        CO.CommandType = CommandType.Text
                        CommandText = "INSERT INTO [UML_CMS].[dbo].[tblgassupplydetails]"
                        CommandText &= "([siteid],[nominatedkwh],[ctrctstatus]) "
                        CommandText &= "VALUES("
                        CommandText &= siteid & ", '" & AllUnitUsage & "', " & tenderstatusid & ");"
                        CommandText &= "SELECT @@IDENTITY;"
                        CO = New SqlCommand(CommandText, myConnection)
                        supplydetailid = CInt(CO.ExecuteScalar)
                    Else
                        Dim kwh As Double = -1
                        CO = New SqlCommand
                        CO.CommandType = CommandType.Text
                        CommandText = "SELECT nominatedkwh FROM [UML_CMS].[dbo].[tblgassupplydetails]"
                        CommandText &= " WHERE [gassupplyid] = " & supplydetailid
                        CO = New SqlCommand(CommandText, myConnection)
                        kwh = CDbl(CO.ExecuteScalar)
                        If AllUnitUsage <> kwh And AllUnitUsage > 0 Then
                            CO = New SqlCommand
                            CO.CommandType = CommandType.Text
                            CommandText = "  UPDATE [UML_CMS].[dbo].[tblgassupplydetails]"
                            CommandText &= " SET [nominatedkwh] = '" & AllUnitUsage & "'"
                            CommandText &= " WHERE [gassupplyid] = " & supplydetailid
                            CO = New SqlCommand(CommandText, myConnection)
                            CO.ExecuteNonQuery()
                        End If
                    End If
                Else

                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "SELECT ID "
                    CommandText &= " FROM dbo.tbl_Purchasing_BasketAdministration  "
                    CommandText &= " WHERE iType = 'E'"
                    CommandText &= " AND BasketDescription = '" & tendertype & "'"
                    CO = New SqlCommand(CommandText, myConnection)
                    tenderproductid = CInt(CO.ExecuteScalar)



                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "INSERT INTO [UML_CMS].[dbo].[tbltenders]"
                    CommandText &= "([siteid],[tenderdesc],[rensupplierid], [FKtenderproduct],[newsupplierstdate], [tenderstatus], [wonlost], [tenderclsddate]) "
                    CommandText &= " VALUES "
                    CommandText &= " (" & siteid & ", '" & ImportDescription & "', " & supplierid & ", " & tenderproductid & ", '" & CDate(RenewalDate).ToString("dd MMM yyyy") & "', 1, 1, '" & CDate(strnow).ToString("dd MMM yyyy") & "');"
                    CommandText &= "SELECT @@IDENTITY;"
                    CO = New SqlCommand(CommandText, myConnection)
                    tenderid = CInt(CO.ExecuteScalar)

                    If StandingCharge = "" Then : StandingCharge = 0 : End If
                    If Not IsNumeric(Left(StandingCharge, 1)) Then
                        StandingCharge = StandingCharge.Replace(Left(StandingCharge, 1), "")

                    End If

                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "INSERT INTO [UML_CMS].[dbo].[tblrates]"
                    CommandText &= "([tenderid],[supplierid],[stgchg],[ctrctlength],[pricetype],[supplierfixedcomm],[suppliercommision]) "
                    CommandText &= " VALUES "
                    CommandText &= " (" & tenderid & ", " & supplierid & ", '" & StandingCharge & "', 12, 'Fully Fixed','" & FixedCommission & "', '" & Commission & "');"
                    CommandText &= "SELECT @@IDENTITY;"
                    CO = New SqlCommand(CommandText, myConnection)
                    rateid = CInt(CO.ExecuteScalar)

                    '/* ******* HACK START ******* */

                    Dim dayband As Integer = -1
                    Dim nightband As Integer = -1
                    Dim allband As Integer = -1


                    Select Case supplierid
                        Case 7  'Scottish and Southern
                            If AllUnitRate = "" Then : dayband = 18 : nightband = 19 : End If
                            If DayRate = "" Then : allband = 95 : End If
                        Case 11 'nPower
                            If MeterTypeElec = "HH" And AllUnitRate = "" Then : dayband = 20 : nightband = 21 : End If
                            If MeterTypeElec = "NHH" And AllUnitRate = "" Then : dayband = 20 : nightband = 21 : End If

                        Case 13 'Eon
                            If MeterTypeElec = "HH" And DayRate <> "" Then : dayband = 339 : nightband = 340 : End If
                            If MeterTypeElec = "HH" And DayRate = "" Then : allband = 268 : End If
                            If MeterTypeElec = "NHH" And AllUnitRate = "" Then : dayband = 339 : nightband = 340 : End If
                            If MeterTypeElec = "NHH" And DayRate = "" Then : allband = 268 : End If
                        Case 19 'Scottish Power
                            If MeterTypeElec = "HH" And AllUnitRate = "" Then : dayband = 63 : nightband = 64 : End If
                            If MeterTypeElec = "HH" And DayRate = "" Then : allband = 102 : End If
                            If MeterTypeElec = "NHH" And AllUnitRate = "" Then : dayband = 63 : nightband = 64 : End If
                            If MeterTypeElec = "NHH" And DayRate = "" Then : allband = 102 : End If
                        Case 22 'Gazrprom
                            If MeterTypeElec = "NHH" And AllUnitRate = "" Then : dayband = 709 : nightband = 710 : End If
                            If MeterTypeElec = "NHH" And DayRate = "" Then : allband = 711 : End If
                        Case 29 'British Gas
                            If MeterTypeElec = "N/A" And AllUnitRate = "" Then : dayband = 82 : nightband = 83 : End If
                            If MeterTypeElec = "N/A" And DayRate = "" Then : allband = 79 : End If
                            If MeterTypeElec = "NHH" And DayUsage > 0 Then : dayband = 82 : nightband = 83 : End If
                            If MeterTypeElec = "NHH" And AllUnitUsage > 0 Then : allband = 79 : End If
                            If MeterTypeElec = "HH" And DayUsage > 0 Then : dayband = 82 : nightband = 83 : End If
                            If MeterTypeElec = "HH" And AllUnitUsage > 0 Then : allband = 79 : End If
                        Case 31 'Total Gas & Power
                            If AllUnitRate = "0" Then : dayband = 199 : nightband = 200 : End If
                            If DayRate = "0" Then : allband = 349 : End If
                        Case 64 'OPUS
                            If MeterTypeElec = "HH" And AllUnitRate = "" Then : dayband = 590 : nightband = 591 : End If
                            If MeterTypeElec = "HH" And DayRate = "" Then : allband = 808 : End If
                            If MeterTypeElec = "NHH" And AllUnitRate = "" Then : dayband = 590 : nightband = 591 : End If
                            If MeterTypeElec = "NHH" And DayRate = "" Then : allband = 808 : End If
                        Case Else
                            System.Diagnostics.Debug.WriteLine("Missing bands for supplier: " & supplierid)
                            Me.lbloutput.Text &= "<br />" & "Missing bands for supplier: " & supplierid
                    End Select















                    '/* ******* HACK END ******* */



                    If Not dayband = -1 Or Not allband = -1 Then

                        If allband = -1 Then

                            CO = New SqlCommand
                            CO.CommandType = CommandType.Text
                            CommandText = "INSERT INTO [UML_CMS].[dbo].[tblbandrate]"
                            CommandText &= "([rateid],[bandid],[rate],[consumption],[lafconsumption]) "
                            CommandText &= " VALUES "
                            CommandText &= " (" & rateid & ", " & dayband & ", '" & DayRate & "', '" & DayUsage & "', 0);"
                            CommandText &= "SELECT @@IDENTITY;"
                            CO = New SqlCommand(CommandText, myConnection)
                            CO.ExecuteNonQuery()

                            CO = New SqlCommand
                            CO.CommandType = CommandType.Text
                            CommandText = "INSERT INTO [UML_CMS].[dbo].[tblbandrate]"
                            CommandText &= "([rateid],[bandid],[rate],[consumption],[lafconsumption]) "
                            CommandText &= " VALUES "
                            CommandText &= " (" & rateid & ", " & nightband & ", '" & NightRate & "', '" & NightUsage & "', 0);"
                            CommandText &= "SELECT @@IDENTITY;"
                            CO = New SqlCommand(CommandText, myConnection)
                            CO.ExecuteNonQuery()
                        Else
                            CO = New SqlCommand
                            CO.CommandType = CommandType.Text
                            CommandText = "INSERT INTO [UML_CMS].[dbo].[tblbandrate]"
                            CommandText &= "([rateid],[bandid],[rate],[consumption],[lafconsumption]) "
                            CommandText &= " VALUES "
                            CommandText &= " (" & rateid & ", " & allband & ", '" & AllUnitRate & "', '" & CInt(AllUnitUsage) & "', 0);"
                            CommandText &= "SELECT @@IDENTITY;"
                            CO = New SqlCommand(CommandText, myConnection)
                            CO.ExecuteNonQuery()
                        End If
                    Else
                        'no day or all units

                    End If

                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "INSERT INTO [UML_CMS].[dbo].[tblelecratehistory]"
                    CommandText &= "([siteid],[rateid],[tenderid],[ratestdate],[rateenddate],[ctrctacnum],[dummyctrct],[regconfirmed]) "
                    CommandText &= "VALUES("
                    CommandText &= siteid & ", " & rateid & ", " & tenderid & ", '" & CDate(RenewalDate).ToString("dd MMM yyyy") & "', '" & CDate(EndDate).ToString("dd MMM yyyy") & "',  '" & ImportDescription & "', 0"
                    If registrationconfirmed = "Yes" Then
                        CommandText &= ",1);"
                    Else
                        CommandText &= ",0);"
                    End If
                    CommandText &= "SELECT @@IDENTITY;"
                    CO = New SqlCommand(CommandText, myConnection)
                    contractid = CInt(CO.ExecuteScalar)
                End If
            End If

        End If
        Dim complete As Boolean = True
        myConnection.Close()

    End Sub

    Protected Sub btnRunImport_Click(sender As Object, e As EventArgs) Handles btnRunImport.Click
        LoadX()
    End Sub
End Class
