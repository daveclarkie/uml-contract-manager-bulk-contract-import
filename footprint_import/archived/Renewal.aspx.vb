Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class _Renewal
    Inherits System.Web.UI.Page

    Dim strFile As String = "data\Staples HH Bulk upload b.csv"
    Dim strConnection As String = "Data Source=uml-sql\umlsql2005;Initial Catalog=UML_CMS;User Id=sa;Password=chicago;"
    Dim CommandText As String = ""
    Dim myConnection As SqlConnection
    Dim CO As SqlCommand

    Dim ImportDescription As String = Left("Renewal Import - " & strFile.ToString.Replace("data\", ""), 50)

    Dim strnow As String = Now.Date.ToString("dd MMM yyyy")

    Dim UMLSiteReference As String = ""
    Dim FuelType As String = ""
    Dim MeterTypeElec As String = ""
    Dim StartDate As String = ""
    Dim EndDate As String = ""
    Dim SupplierName As String = ""
    Dim AllUnitRate As String = ""
    Dim DayRate As String = ""
    Dim NightRate As String = ""
    Dim StandingCharge As String = ""
    Dim AllUnitUsage As String = ""
    Dim DayUsage As String = ""
    Dim NightUsage As String = ""
    Dim Commission As String = ""
    Dim FixedCommission As String = ""
    Dim ContractType As String = ""
    Dim TenderStatus As String = ""
    Dim TerminationOptOut As String = ""
    Dim RegistrationConfirmed As String = ""
    Dim KvAChargeMonth As String = ""

    Dim mCSV As New CSVData

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.lbloutput.Text = ImportDescription

        Try
            ResetVariables()

        Catch aa As Exception
            System.Diagnostics.Debug.WriteLine(aa.Message.ToString)
        End Try


    End Sub

    Public Sub LoadX()
        '

        ' Set the separator and text qualifier

        mCSV.Separator = ","
        mCSV.TextQualifier = """"
        '
        ' Load in the CSV
        '
        mCSV.LoadCSV(Server.MapPath(strFile), True)
        '
        ' Clear the listview of any data and columns
        '
        Dim maxcol As Integer = mCSV.CSVDataSet.Tables(0).Columns.Count - 1
        Dim dt As DataTable = mCSV.CSVDataSet.Tables(0)
        ProcessBulkDataTable(dt)

    End Sub

    Private Sub ResetVariables()

        UMLSiteReference = ""
        FuelType = ""
        MeterTypeElec = ""
        StartDate = ""
        EndDate = ""
        SupplierName = ""
        AllUnitRate = ""
        DayRate = ""
        NightRate = ""
        StandingCharge = ""
        AllUnitUsage = ""
        DayUsage = ""
        NightUsage = ""
        Commission = ""
        FixedCommission = ""
        ContractType = ""
        TenderStatus = ""
        TerminationOptOut = ""
        RegistrationConfirmed = ""
        KvAChargeMonth = ""

    End Sub

    Private Function getfirst(ByVal str As String) As String
        Dim out As String = ""

        Dim chrPos As Integer = 0
        Dim chrMax As Integer = str.Length

        Do While chrPos < chrMax
            If chrPos > 0 Then
                If Char.IsUpper(str, chrPos) Then
                    If Char.IsLower(str, chrPos - 1) Then
                        Exit Do
                    End If
                    If str(chrPos - 1).ToString = ")" Then
                        Exit Do
                    End If
                    If Char.IsDigit(str, chrPos - 1) Then
                        If Not Char.IsWhiteSpace(str, chrPos + 1) Then
                            Exit Do
                        End If
                    End If
                End If
                If Char.IsDigit(str, chrPos) Then
                    If Char.IsLower(str, chrPos - 1) Then
                        Exit Do
                    End If
                End If
                If str(chrPos).ToString = "," Then
                    Exit Do
                End If
            End If
            out += str(chrPos).ToString
            chrPos += 1
        Loop
        Return out
    End Function

    Private Sub ProcessBulkDataTable(ByVal dt As DataTable)

        Dim dr As DataRow
        Dim iRowIndex As Integer = 0
        For Each dr In dt.Rows
            iRowIndex += 1
            ResetVariables()

            UMLSiteReference = dr("UML_Site_Reference").ToString.Replace("'", "")
            FuelType = dr("Fuel_Type").ToString.Replace("'", "")
            MeterTypeElec = dr("Meter_Type_Elec").ToString.Replace("'", "")
            StartDate = dr("Start Date").ToString.Replace("'", "")
            EndDate = dr("End Date").ToString.Replace("'", "")
            SupplierName = dr("Supplier_Name").ToString.Replace("'", "")
            AllUnitRate = dr("All Unit Rate").ToString.Replace("'", "")
            DayRate = dr("Day Rate").ToString.Replace("'", "")
            NightRate = dr("Night Rate").ToString.Replace("'", "")
            StandingCharge = dr("standing charge month").ToString.Replace("'", "")
            AllUnitUsage = dr("All Unit Usage").ToString.Replace("'", "")
            DayUsage = dr("Day Usage").ToString.Replace("'", "")
            NightUsage = dr("Night Usage").ToString.Replace("'", "")
            Commission = dr("Commission p kwh").ToString.Replace("'", "")
            'FixedCommission = dr("Fixed Commission Per year").ToString.Replace("'", "")
            ContractType = dr("Contract Type").ToString.Replace("'", "")
            TenderStatus = dr("Tender Status").ToString.Replace("'", "")
            TerminationOptOut = dr("Termination Opt Out").ToString.Replace("'", "")
            RegistrationConfirmed = dr("Registration Confirmed").ToString.Replace("'", "")
            KvAChargeMonth = dr("KvA Charge").ToString.Replace("'", "")

            SetVariables()

            Me.lbloutput.Text &= "<br />" & iRowIndex & " | " & UMLSiteReference & " | " & StartDate & " - " & EndDate & " (" & iRowIndex & "/" & dt.Rows.Count & ")"

        Next

    End Sub

    Private Sub SetVariables()


        myConnection = New SqlConnection(strConnection)
        myConnection.Open()
        CO = New SqlCommand

        Dim custid As Integer = -1
        Dim siteid As Integer = -1

        If Not UMLSiteReference = "" Or Not UMLSiteReference = "N/A" Then
            Try
                siteid = UMLSiteReference
                CO.CommandType = CommandType.Text
                CommandText = "SELECT custid FROM tblSites WHERE siteid = " & siteid
                CO = New SqlCommand(CommandText, myConnection)
                custid = CInt(CO.ExecuteScalar)
            Catch ex As Exception
            End Try
        End If

        '   if gas / elec
        '       elec: elec contract
        '       gas:  gas contract

        Dim supplierid As Integer = -1
        Dim tenderid As Integer = -1
        Dim rateid As Integer = -1
        Dim contractid As Integer = -1
        Dim supplydetailid As Integer = -1

        CommandText = "SELECT TOP 1 SupplierId FROM [UML_CMS].[dbo].[tblsuppliers]"
        CommandText &= " WHERE Supplier_name = '" & SupplierName & "'"
        CO = New SqlCommand(CommandText, myConnection)
        supplierid = CInt(CO.ExecuteScalar)

        Select Case supplierid
            Case Nothing
                supplierid = 42
        End Select


        If FuelType.ToLower = "gas" Then
            CommandText = "SELECT PKgasratehistid FROM [UML_CMS].[dbo].[tblgasratehistory]"
            CommandText &= " WHERE fksiteid = " & siteid
            CommandText &= " and ratestdate = '" & CDate(StartDate).ToString("dd MMM yyyy") & "'"
            CommandText &= " and ctrctacnum = '" & ImportDescription & "'"
            CO = New SqlCommand(CommandText, myConnection)
            contractid = CInt(CO.ExecuteScalar)
        Else
            CommandText = "SELECT ratehistid FROM [UML_CMS].[dbo].[tblelecratehistory]"
            CommandText &= " WHERE siteid = " & siteid
            CommandText &= " and ratestdate = '" & CDate(StartDate).ToString("dd MMM yyyy") & "'"
            CommandText &= " and ctrctacnum = '" & ImportDescription & "'"
            CO = New SqlCommand(CommandText, myConnection)
            contractid = CInt(CO.ExecuteScalar)
        End If

        'Remove "," from usage
        DayUsage = DayUsage.Replace(",", "")
        NightUsage = NightUsage.Replace(",", "")
        AllUnitUsage = AllUnitUsage.Replace(",", "")

        If DayUsage = "N/A" Or DayUsage = "" Then : DayUsage = 0 : End If
        If NightUsage = "N/A" Or NightUsage = "" Then : NightUsage = 0 : End If
        If AllUnitUsage = "N/A" Or AllUnitUsage = "" Then : AllUnitUsage = 0 : End If

        If DayUsage.Length > 0 And Not IsNumeric(Left(DayUsage, 1)) Then : DayUsage = DayUsage.Replace(Left(DayUsage, 1), "") : End If
        If NightUsage.Length > 0 And Not IsNumeric(Left(NightUsage, 1)) Then : NightUsage = NightUsage.Replace(Left(NightUsage, 1), "") : End If
        If AllUnitUsage.Length > 0 And Not IsNumeric(Left(AllUnitUsage, 1)) Then : AllUnitUsage = AllUnitUsage.Replace(Left(AllUnitUsage, 1), "") : End If

        DayUsage = CInt(DayUsage)
        NightUsage = CInt(NightUsage)
        AllUnitUsage = CInt(AllUnitUsage)


        If AllUnitRate.Length = 0 Then AllUnitRate = "0"
        If StandingCharge.Length = 0 Then StandingCharge = "0"
        If Commission.Length = 0 Then Commission = "0"
        If FixedCommission.Length = 0 Then FixedCommission = "0"

        Dim tenderproductid As Integer = -1
        If contractid < 1 Then
            If FuelType.ToLower = "gas" Then

                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "SELECT ID "
                CommandText &= " FROM dbo.tbl_Purchasing_BasketAdministration  "
                CommandText &= " WHERE iType = 'G'"
                CommandText &= " AND BasketDescription = '" & ContractType & "'"
                CO = New SqlCommand(CommandText, myConnection)
                tenderproductid = CInt(CO.ExecuteScalar)


                CO = New SqlCommand
                CommandText = "INSERT INTO [UML_CMS].[dbo].[tblgastenders]"
                CommandText &= "([siteid],[tenderdesc], [tenderstatus], [wonlost], [tenderclsddate], [FKtenderproduct])"

                CommandText &= " VALUES "
                CommandText &= " (" & siteid & ", '" & ImportDescription & "', 1, 1, '" & CDate(strnow).ToString("dd MMM yyyy") & "', " & tenderproductid & ");"
                CommandText &= "SELECT @@IDENTITY;"
                CO = New SqlCommand(CommandText, myConnection)
                tenderid = CInt(CO.ExecuteScalar)

                If AllUnitRate.Length = 0 Then AllUnitRate = "0"
                If StandingCharge.Length = 0 Then StandingCharge = "0"
                If Commission.Length = 0 Then Commission = "0"
                If FixedCommission.Length = 0 Then FixedCommission = "0"


                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "INSERT INTO [UML_CMS].[dbo].[tblgasrates]"
                CommandText &= "([gastenderid],[supplierid],[gastendrate],[ctrctlength],[gasmeterchgpa],[suppliercommision],[supplierfixedcomm]) "
                CommandText &= " VALUES "
                CommandText &= " (" & tenderid & ", " & supplierid & ", '" & AllUnitRate & "', DATEDIFF(MONTH, '" & CDate(StartDate).ToString("dd MMM yyyy") & "','" & CDate(EndDate).ToString("dd MMM yyyy") & "')+1, '" & StandingCharge & "', '" & Commission & "','" & CInt(FixedCommission) & "');"
                CommandText &= "SELECT @@IDENTITY;"
                CO = New SqlCommand(CommandText, myConnection)
                rateid = CInt(CO.ExecuteScalar)

                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "INSERT INTO [UML_CMS].[dbo].[tblgasratehistory]"
                CommandText &= "([fksiteid],[rateid],[tenderid],[ratestdate],[rateenddate],[ctrctacnum],[regconfirmed],[intAutoTerminated]) "
                CommandText &= "VALUES("
                CommandText &= siteid & ", " & rateid & ", " & tenderid & ", '" & CDate(StartDate).ToString("dd MMM yyyy") & "', '" & CDate(EndDate).ToString("dd MMM yyyy") & "','" & ImportDescription & "'"
                If RegistrationConfirmed = "Yes" Then
                    CommandText &= ",1"
                Else
                    CommandText &= ",0"
                End If

                If TerminationOptOut = "Yes" Then
                    CommandText &= ",1);"
                Else
                    CommandText &= ",0);"
                End If
                CommandText &= "SELECT @@IDENTITY;"
                CO = New SqlCommand(CommandText, myConnection)
                contractid = CInt(CO.ExecuteScalar)

                'supply detail already exists for site
                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "SELECT gassupplyid FROM [UML_CMS].[dbo].[tblgassupplydetails]"
                CommandText &= " WHERE [siteid] = " & siteid
                CO = New SqlCommand(CommandText, myConnection)
                supplydetailid = CInt(CO.ExecuteScalar)

                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "SELECT StatusID FROM [UML_CMS].[dbo].[tbltenderstatus] "
                CommandText &= " WHERE [StatusMain] = '" & TenderStatus & "'"
                CO = New SqlCommand(CommandText, myConnection)
                Dim tenderstatusid As Integer = -1
                tenderstatusid = CInt(CO.ExecuteScalar)


                If supplydetailid = 0 Then
                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "INSERT INTO [UML_CMS].[dbo].[tblgassupplydetails]"
                    CommandText &= "([siteid],[nominatedkwh],[ctrctstatus]) "
                    CommandText &= "VALUES("
                    CommandText &= siteid & ", '" & AllUnitUsage & "', " & tenderstatusid & ");"
                    CommandText &= "SELECT @@IDENTITY;"
                    CO = New SqlCommand(CommandText, myConnection)
                    supplydetailid = CInt(CO.ExecuteScalar)
                Else
                    Dim kwh As Double = -1
                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CommandText = "SELECT ISNULL(nominatedkwh,0) FROM [UML_CMS].[dbo].[tblgassupplydetails]"
                    CommandText &= " WHERE [gassupplyid] = " & supplydetailid
                    CO = New SqlCommand(CommandText, myConnection)
                    kwh = CDbl(CO.ExecuteScalar)
                    If AllUnitUsage <> kwh And AllUnitUsage > 0 Then
                        CO = New SqlCommand
                        CO.CommandType = CommandType.Text
                        CommandText = "  UPDATE [UML_CMS].[dbo].[tblgassupplydetails]"
                        CommandText &= " SET [nominatedkwh] = '" & AllUnitUsage & "'"
                        CommandText &= " WHERE [gassupplyid] = " & supplydetailid
                        CO = New SqlCommand(CommandText, myConnection)
                        CO.ExecuteNonQuery()
                    End If
                End If
            Else

                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "SELECT ID "
                CommandText &= " FROM dbo.tbl_Purchasing_BasketAdministration  "
                CommandText &= " WHERE iType = 'E'"
                CommandText &= " AND BasketDescription = '" & ContractType & "'"
                CO = New SqlCommand(CommandText, myConnection)
                tenderproductid = CInt(CO.ExecuteScalar)



                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "INSERT INTO [UML_CMS].[dbo].[tbltenders]"
                CommandText &= "([siteid],[tenderdesc],[rensupplierid], [FKtenderproduct],[newsupplierstdate], [tenderstatus], [wonlost], [tenderclsddate]) "
                CommandText &= " VALUES "
                CommandText &= " (" & siteid & ", '" & ImportDescription & "', " & supplierid & ", " & tenderproductid & ", '" & CDate(StartDate).ToString("dd MMM yyyy") & "', 1, 1, '" & CDate(strnow).ToString("dd MMM yyyy") & "');"
                CommandText &= "SELECT @@IDENTITY;"
                CO = New SqlCommand(CommandText, myConnection)
                tenderid = CInt(CO.ExecuteScalar)
                
                If StandingCharge = "" Then
                    StandingCharge = 0
                ElseIf StandingCharge = "-" Then
                    StandingCharge = 0
                End If
                If Not IsNumeric(Left(StandingCharge, 1)) Then
                    StandingCharge = StandingCharge.Replace(Left(StandingCharge, 1), "")

                End If

                '/* ******* HACK START ******* */


                Dim dayband As Integer = -1
                Dim nightband As Integer = -1
                Dim allband As Integer = -1
                Dim stodid As Integer = -1

                'nPower
                If supplierid = 11 And MeterTypeElec = "HH" And DayUsage > 0 Then : dayband = 20 : nightband = 21 : stodid = 17 : End If
                If supplierid = 11 And MeterTypeElec = "NHH" And DayUsage > 0 Then : dayband = 20 : nightband = 21 : stodid = 17 : End If

                'British Gas
                If supplierid = 29 And MeterTypeElec = "N/A" And DayUsage > 0 Then : dayband = 82 : nightband = 83 : stodid = 46 : End If
                If supplierid = 29 And MeterTypeElec = "N/A" And AllUnitUsage > 0 Then : allband = 79 : stodid = 44 : End If

                If supplierid = 29 And MeterTypeElec = "NHH" And DayUsage > 0 Then : dayband = 82 : nightband = 83 : stodid = 46 : End If
                If supplierid = 29 And MeterTypeElec = "NHH" And AllUnitUsage > 0 Then : allband = 79 : stodid = 44 : End If

                If supplierid = 29 And MeterTypeElec = "HH" And DayUsage > 0 Then : dayband = 82 : nightband = 83 : stodid = 46 : End If
                If supplierid = 29 And MeterTypeElec = "HH" And AllUnitUsage > 0 Then : allband = 79 : stodid = 44 : End If

                'Gazrprom
                If supplierid = 22 And MeterTypeElec = "NHH" And DayUsage > 0 Then : dayband = 709 : nightband = 710 : stodid = 283 : End If
                If supplierid = 22 And MeterTypeElec = "NHH" And AllUnitUsage > 0 Then : allband = 711 : stodid = 284 : End If

                'Scottish and Southern
                If supplierid = 7 And DayUsage > 0 Then : dayband = 18 : nightband = 19 : stodid = 16 : End If
                If supplierid = 7 And AllUnitUsage > 0 Then : allband = 95 : stodid = 53 : End If
                
                ' Hudson Energy
                If supplierid = 91 And DayUsage > 0 Then : dayband = 896 : nightband = 897 : stodid = 360 : End If
                If supplierid = 91 And AllUnitUsage > 0 Then : allband = 900 : stodid = 359 : End If

                ' EON
                If supplierid = 13 And DayUsage > 0 Then : dayband = 339 : nightband = 340 : stodid = 146 : End If
                If supplierid = 13 And AllUnitUsage > 0 Then : allband = 268 : stodid = 118 : End If


                '/* ******* HACK END ******* */

                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "INSERT INTO [UML_CMS].[dbo].[tblrates]"
                CommandText &= "([tenderid],[supplierid],[stgchg],[ctrctlength],[pricetype],[suppliercommision],[stodid],[supplierfixedcomm],[kVAcharge]) "
                CommandText &= " VALUES "
                CommandText &= " (" & tenderid & ", " & supplierid & ", '" & StandingCharge & "', 12, 'Fully Fixed','" & Commission & "', " & stodid & ",'" & FixedCommission & "','" & KvAChargeMonth & "');"
                CommandText &= "SELECT @@IDENTITY;"
                CO = New SqlCommand(CommandText, myConnection)
                rateid = CInt(CO.ExecuteScalar)





                If Not dayband = -1 Or Not allband = -1 Then

                    If allband = -1 Then

                        CO = New SqlCommand
                        CO.CommandType = CommandType.Text
                        CommandText = "INSERT INTO [UML_CMS].[dbo].[tblbandrate]"
                        CommandText &= "([rateid],[bandid],[rate],[consumption],[lafconsumption]) "
                        CommandText &= " VALUES "
                        CommandText &= " (" & rateid & ", " & dayband & ", '" & DayRate & "', '" & DayUsage & "', 0);"
                        CommandText &= "SELECT @@IDENTITY;"
                        CO = New SqlCommand(CommandText, myConnection)
                        CO.ExecuteNonQuery()

                        CO = New SqlCommand
                        CO.CommandType = CommandType.Text
                        CommandText = "INSERT INTO [UML_CMS].[dbo].[tblbandrate]"
                        CommandText &= "([rateid],[bandid],[rate],[consumption],[lafconsumption]) "
                        CommandText &= " VALUES "
                        CommandText &= " (" & rateid & ", " & nightband & ", '" & NightRate & "', '" & NightUsage & "', 0);"
                        CommandText &= "SELECT @@IDENTITY;"
                        CO = New SqlCommand(CommandText, myConnection)
                        CO.ExecuteNonQuery()
                    Else
                        CO = New SqlCommand
                        CO.CommandType = CommandType.Text
                        CommandText = "INSERT INTO [UML_CMS].[dbo].[tblbandrate]"
                        CommandText &= "([rateid],[bandid],[rate],[consumption],[lafconsumption]) "
                        CommandText &= " VALUES "
                        CommandText &= " (" & rateid & ", " & allband & ", '" & AllUnitRate & "', '" & CInt(AllUnitUsage) & "', 0);"
                        CommandText &= "SELECT @@IDENTITY;"
                        CO = New SqlCommand(CommandText, myConnection)
                        CO.ExecuteNonQuery()
                    End If
                Else
                    'no day or all units

                End If

                CO = New SqlCommand
                CO.CommandType = CommandType.Text
                CommandText = "INSERT INTO [UML_CMS].[dbo].[tblelecratehistory]"
                CommandText &= "([siteid],[rateid],[tenderid],[ratestdate],[rateenddate],[ctrctacnum],[dummyctrct],[regconfirmed],[intAutoTerminated]) "
                CommandText &= "VALUES("
                CommandText &= siteid & ", " & rateid & ", " & tenderid & ", '" & CDate(StartDate).ToString("dd MMM yyyy") & "', '" & CDate(EndDate).ToString("dd MMM yyyy") & "',  '" & ImportDescription & "', 0"
                If RegistrationConfirmed = "Yes" Then
                    CommandText &= ",1"
                Else
                    CommandText &= ",0"
                End If

                If TerminationOptOut = "Yes" Then
                    CommandText &= ",1);"
                Else
                    CommandText &= ",0);"
                End If


                CommandText &= "SELECT @@IDENTITY;"
                CO = New SqlCommand(CommandText, myConnection)
                contractid = CInt(CO.ExecuteScalar)
            End If
        End If

        Dim complete As Boolean = True
        myConnection.Close()

    End Sub

    Protected Sub btnRunImport_Click(sender As Object, e As EventArgs) Handles btnRunImport.Click
        LoadX()
    End Sub
End Class
