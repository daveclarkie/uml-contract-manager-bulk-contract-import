<%@ Page Language="VB" AutoEventWireup="false" CodeFile="helpdesk_budgetupload.aspx.vb" Inherits="helpdesk_budgetupload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Import - Contract Elec</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Customers Before Import:      <asp:Label ID="lblBeforeCustomers" runat="server" Text="0"></asp:Label><br />
        <br />
        Customers After Import:       <asp:Label ID="lblAfterCustomers" runat="server" Text="0"></asp:Label><br />
        <br />

        <asp:Button ID="btnRunImport" runat="server" Text="Run Import" /><br />
        <br />
        
        <asp:Label ID="lbloutput" runat="server"></asp:Label>        

        
    </div>
    </form>
</body>
</html>
